import com.distribuidos.healthpotions.dto.order.OrderWithDetailsDTO
import com.healthpotions.resources.category.CategoryResourceImpl
import com.healthpotions.resources.client.ClientResourceImpl
import com.healthpotions.resources.contact.ContactResourceImpl
import com.healthpotions.resources.order.OrderDetailResourceImpl
import com.healthpotions.resources.order.OrderResourceImpl
import com.healthpotions.resources.order.OrderWithDetailsResource
import com.healthpotions.resources.personal_data.PersonalDataResource
import com.healthpotions.resources.product.ProductResourceImpl
import com.healthpotions.resources.purchase_invoice.PurchaseInvoiceDetailResource
import com.healthpotions.resources.purchase_invoice.PurchaseInvoiceResource
import com.healthpotions.resources.purchase_invoice.PurchaseInvoiceWithDetailsResource
import com.healthpotions.resources.role.RoleResource
import com.healthpotions.resources.saleInvoice.SaleInvoiceDetailResourceImpl
import com.healthpotions.resources.saleInvoice.SaleInvoiceResourceImpl
import com.healthpotions.resources.supplier.SupplierResourceImpl
import com.healthpotions.resources.type.TypeResourceImpl
import com.healthpotions.resources.user.UserResource
import com.healthpotions.services.category.CategoryServiceImpl
import com.healthpotions.services.client.ClientServiceImpl
import com.healthpotions.services.contact.ContactServiceImpl
import com.healthpotions.services.order.OrderDetailServiceImpl
import com.healthpotions.services.order.OrderServiceImpl
import com.healthpotions.services.personal_data.PersonalDataService
import com.healthpotions.services.product.ProductService
import com.healthpotions.services.purchase_invoice.PurchaseInvoiceDetailService
import com.healthpotions.services.purchase_invoice.PurchaseInvoiceService
import com.healthpotions.services.role.RoleService
import com.healthpotions.services.saleInvoice.SaleInvoiceDetailService
import com.healthpotions.services.saleInvoice.SaleInvoiceService
import com.healthpotions.services.supplier.SupplierServiceImpl
import com.healthpotions.services.type.TypeService
import com.healthpotions.services.user.UserService

beans = {

    //services
    typeService(TypeService)
    personalDataService(PersonalDataService)
    productService(ProductService)
    categoryService(CategoryServiceImpl)
    saleInvoiceService(SaleInvoiceService)
    saleInvoiceDetailService(SaleInvoiceDetailService)
    userService(UserService)
    roleService(RoleService)
    purchaseInvoiceDetailService(PurchaseInvoiceDetailService)
    purchaseInvoiceService(PurchaseInvoiceService)
    clientService(ClientServiceImpl)
    supplierService(SupplierServiceImpl)
    contactService(ContactServiceImpl)
    orderService(OrderServiceImpl)
    orderDetailService(OrderDetailServiceImpl)


    //resources
    typeResource(TypeResourceImpl)
    personalDataResource(PersonalDataResource)
    productResource(ProductResourceImpl)
    categoryResource(CategoryResourceImpl)
    saleInvoiceResource(SaleInvoiceResourceImpl)
    saleInvoiceDetailResource(SaleInvoiceDetailResourceImpl)
    userResource(UserResource)
    roleResource(RoleResource)
    orderResource(OrderResourceImpl)
    orderDetailResource(OrderDetailResourceImpl)
    orderWithDetailResource(OrderWithDetailsResource)
    purchaseInvoiceWithDetailsResource(PurchaseInvoiceWithDetailsResource)
    purchaseInvoiceResource(PurchaseInvoiceResource)
    purchaseInvoiceDetailResource(PurchaseInvoiceDetailResource)
    clientResource(ClientResourceImpl)
    supplierResource(SupplierResourceImpl)
    contactResource(ContactResourceImpl)
}
