<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'personalData.label', default: 'Personal Data')}" />
        <title><g:message code="default.create.label"/> :: Heathpotions</title>
    </head>
    <body>
%{--        <div class="nav" role="navigation">--}%
%{--            <ul>--}%
%{--                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--            </ul>--}%
%{--        </div>--}%
        <div id="create-personalData" class="container" role="main">
            <h1><g:message code="default.create.label" args="[entityName]" /></h1>
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.personalData}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.personalData}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>

        <g:form action="save" >
            <fieldset class="form">
                <g:render template="form"/>
            </fieldset>
            <br>
            <div class="container-fluid col-6">
                <fieldset class="buttons">
                    <a href="index" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Volver"/></a>
                    <g:submitButton name="create" class="save btn-primary btn-sm" value="${message(code: 'default.button.create.label', default: 'Create')}" />
                </fieldset>
            </div>
        </g:form>

    </div>
    </body>
</html>
