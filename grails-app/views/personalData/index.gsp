<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'personalData.label', default: 'Datos Personales')}"/>
    <title><g:message code="default.list.label"/> :: Healthpotions</title>

    <asset:stylesheet src="personal-data-index.css"/>
    <asset:stylesheet src="index.css"/>
</head>

<body>

%{--        <div class="nav" role="navigation">--}%
%{--            <ul class="navbar-nav">--}%
%{--                <li class="nav-item"><a href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--                <li class="nav-item"><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--            </ul>--}%
%{--        </div>--}%

<div class="container centered">

    <h1 class="title">Datos Personales</h1>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">${flash.message}</div>
    </g:if>

    <div id="spinner">
        <strong>Loading...</strong>

        <div class="spinner-grow" role="status"></div>
    </div>

    <div class="container-fluid buttons hidden" id="buttons">
        <div class="md-1">
            <button class="btn btn-success"><g:link class="link-light" action="create">➕<g:message code="default.new.label"
                                                                                              args="[entityName]"/></g:link></button>
        </div>

        <div class="md-1">
            <label for="query" class="form-label">Buscar:</label>
            <input type="text" class="form-control" id="query">
        </div>
    </div>

    <table class="table hidden " id="table">
        <thead>
        <tr>
            <th scope="col">Nombre</th>
            <th scope="col">Apellido</th>
            <th scope="col">Documento</th>
            <th scope="col">Ruc</th>
            <th scope="col">Direccion</th>
            <th scope="col">Nacimiento</th>
            <th scope="col">Tel</th>
            <th scope="col">Email</th>
            <th scope="col">Acciones</th>
        </tr>
        </thead>
        <tbody id="table-body">
        </tbody>
    </table>

    <nav aria-label="...">
        <ul class="pagination hidden" id="pagination">

            %{--            <li class="page-item"><a class="page-link" href="#">1</a></li>--}%
            %{--            <li class="page-item active" aria-current="page">--}%
            %{--                <a class="page-link" href="#">2</a>--}%
            %{--            </li>--}%
            %{--            <li class="page-item"><a class="page-link" href="#">3</a></li>--}%
            %{--            <li class="page-item">--}%
            %{--                <a class="page-link" href="#">Next</a>--}%
            %{--            </li>--}%
        </ul>
    </nav>
</div>
<asset:javascript src="personal-data-index.js" type="module"/>
</body>
</html>