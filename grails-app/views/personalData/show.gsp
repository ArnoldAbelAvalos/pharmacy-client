<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'personalData.label', default: 'Datos Personales')}"/>
    <title><g:message code="default.show.label"/> :: Healthpotions</title>
</head>

<body>
%{--        <div class="nav" role="navigation">--}%
%{--            <ul>--}%

%{--                <li><g:link class="" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--                <li><g:link class="" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--            </ul>--}%
%{--        </div>--}%
<div id="show-personalData" class="content scaffold-show" role="main">
    <div class="container" style="padding-top: 5%">
        <g:if test="${flash.message}">
            <div class="alert alert-success" role="status">${flash.message}</div>
        </g:if>
        <div class="card mb-3">
        <div class="row g-0">
            <div class="col-md-4">
                <asset:image src="user.png" class="img-fluid rounded-start"/>
            </div>

            <div class="col-md-8">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title">Nombre:</h5>

                            <p class="card-text">${personalDataBean.name}</p>
                            <h5 class="card-title">Apellido:</h5>

                            <p class="card-text">${personalDataBean.lastName}</p>
                            <h5 class="card-title">Documento:</h5>

                            <p class="card-text">${personalDataBean.document}</p>
                            <h5 class="card-title">R.U.C:</h5>

                            <p class="card-text">${personalDataBean.ruc}</p>
                        </div>

                        <div class="col">
                            <h5 class="card-title">Direccion:</h5>

                            <p class="card-text">${personalDataBean.address}</p>
                            <h5 class="card-title">Nacimiento:</h5>

                            <p class="card-text">${personalDataBean.dob}</p>
                            <h5 class="card-title">Tel:</h5>

                            <p class="card-text">${personalDataBean.phone}</p>
                            <h5 class="card-title">Email:</h5>

                            <p class="card-text">${personalDataBean.email}</p>
                        </div>

                    </div>

                </div>
            </div>
        </div>

        <g:form resource="${this.personalData}" method="DELETE" action="delete" id="${personalDataBean?.id}" class="mx-auto">
            <fieldset class="buttons">
                <g:link class="btn btn-warning" action="edit" resource="${this.personalData}" id="${personalDataBean?.id}">🔧<g:message code="default.button.edit.label"
                                                                                              default="Edit"/></g:link>
                <input class="btn btn-danger" type="submit" value="❌${message(code: 'default.button.delete.label', default: 'Delete')}"
                       onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
            </fieldset>
        </g:form>

%{--        <div class="row">--}%
%{--            <g:form resource="${this.personalData}" method="DELETE" action="delete" id="${personalData?.id}">--}%
%{--                <g:submitButton name="submit" class="btn btn-sm btn-outline-danger" value="${message(code: 'default.button.delete.label', default: 'Eliminar')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />--}%
%{--            </g:form>--}%
%{--        </div>--}%
    </div>
</div>
</div>
</body>
</html>
