<div class="container-fluid col-10">
    %{--    <div class="fieldcontain ${hasErrors(bean: personalData, field: 'name', 'lastName', 'document', 'ruc', 'address', 'phone', 'email', 'error')} required">--}%
    <div class="row">
        <div class="col">
            <div>
                <label for="name" class="form-label">Nombre *</label>
                <g:textField name="name" maxlength="30" required="true" class="form-control"
                             value="${personalData?.name}"
                             placeholder="Jhon"/>
            </div>

            <div>
                <label for="lastName" class="form-label">Apellido *</label>
                <g:textField name="lastName" maxlength="30" required="true" class="form-control"
                             value="${personalData?.lastName}" placeholder="Doe"/>
            </div>

            <div>
                <label for="document" class="form-label">Documento *</label>
                <g:field type="number" name="document" min="0" required="true" class="form-control"
                         value="${personalData?.document}" placeholder="0000001"/>
            </div>

            <div>
                <label for="ruc" class="form-label">R.U.C *</label>
                <g:textField name="ruc" maxlength="15" required="true" class="form-control" value="${personalData?.ruc}"
                             placeholder="0000001-1"/>
            </div>
        </div>

        <div class="col">
            <div>
                <label for="address" class="form-label">Direccion *</label>
                <g:textField name="address" maxlength="50" required="true" class="form-control"
                             value="${personalData?.address}"
                             placeholder="Av. 4"/>
            </div>

            <div>
                <label for="dob" class="form-label">Nacimiento *</label>
                <g:field type="date" name="dob" required="true" class="form-control" value="${personalData?.dob}"
                         placeholder=""/>
            </div>

        <div>
            <div>
                <label for="phone" class="form-label">Telefono *</label>
                <g:textField name="phone" maxlength="50" required="true" class="form-control"
                             value="${personalData?.phone}"
                             placeholder="XXXX-XXX-XXX"/>
            </div>

            <div>
                <label for="email" class="form-label">Email *</label>
                <g:field type="email" name="email" maxlength="50" required="true" class="form-control"
                         value="${personalData?.email}" placeholder="admin@gmail.com"/>
            </div>
        </div>

    </div>

</div>
