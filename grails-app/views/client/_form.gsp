<div class="container-fluid col-6">
	<div class="row">
		<h4 class="align-content-center" style="padding-top: 5%">Cliente</h4>
		<hr>
		<div class="col-md-6 bg-light card-body">
			<h6>Datos Personales</h6><hr>
			<div class="form-group row">
				<div class="col-6">
					<label for="name" class="col-6 col-form-label">Nombre*</label>
					<g:textField name="name" maxlength="100" required="true" class="form-control" value="${personalData?.name}"/>
				</div>

				<div class="col-6">
					<label for="lastName" class="col-6 col-form-label">Apellido*</label>
					<g:textField name="lastName" maxlength="100" required="true" class="form-control" value="${personalData?.lastName}"/>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-6">
					<label for="document" class="col-6 col-form-label">Nro. Documento*</label>
					<g:textField name="document" maxlength="100" required="true" class="form-control" value="${personalData?.document}"/>
				</div>

				<div class="col-6">
					<label for="ruc" class="col-6 col-form-label">RUC*</label>
					<g:textField name="ruc" maxlength="100" required="true" class="form-control" value="${personalData?.ruc}"/>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-6">
					<label for="dob" class="col-6 col-form-label">Fecha de Nacimiento*</label>
					<g:field type="date" name="dob" required="true" class="form-control" value="${personalData?.dob}" placeholder=""/>				</div>

				<div class="col-6">
					<label for="address" class="col-6 col-form-label">Dirección*</label>
					<g:textField name="address" maxlength="100" required="true" class="form-control" value="${personalData?.address}"/>
				</div>
			</div>

			<br><h6>Datos de Contacto</h6><hr>
			<div class="form-group row">
				<div class="col-6">
					<label for="phone" class="col-6 col-form-label">Teléfono*</label>
					<g:textField name="phone" maxlength="100" required="true" class="form-control" value="${personalData?.phone}"/>
				</div>

				<div class="col-6">
					<label for="email" class="col-6 col-form-label">Email*</label>
					<g:textField name="email" maxlength="100" required="true" class="form-control" value="${personalData?.email}"/>
				</div>
			</div>
		</div>
	</div>
</div>
