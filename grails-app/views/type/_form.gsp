<div class="container-fluid col-6">
	<div class="row">
		<h4 class="align-content-center" style="padding-top: 5%">Grupo - Tipo de Medicamento</h4>
		<hr>
		<div class="col-md-6 bg-light card-body">
			<div class="fieldcontain ${hasErrors(bean: typeInstance, field: 'name', 'error')} required">
				<label for="name" class="form-label">Nombre *</label>
				<div>
					<g:textField name="name" maxlength="50" required="true" class="form-control" value="${typeInstance?.name}"/>
				</div>
			</div>
		</div>
	</div>
</div>


