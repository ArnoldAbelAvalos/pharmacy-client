<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'type.label', default: 'Type')}" />
    <title>Editar :: Healthpotions</title>
</head>

<body>

<div id="create-type" class="content scaffold-create" role="main">
    <g:hasErrors bean="${this.type}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.type}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.type}" action="update" method="PUT">
        <g:hiddenField name="id" value="${typeInstance?.id}" />
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <br>
        <div class="container-fluid col-6" align="right">
            <fieldset class="buttons">
                <a href="${createLink(uri: '/type/index')}" class="btn btn-outline-dark"><g:message code="default.link.skip.label" default="Atrás"/></a>
                <g:submitButton name="edit" class="btn btn-outline-success" value="${message(code: 'default.button.update.label', default: 'Actualizar')}" />
            </fieldset>
        </div>
    </g:form>
</body>
</html>
