<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title>Lista :: Healthpotions</title>    <asset:stylesheet src="index.css"/>
</head>

<body>
%{--        <div class="nav" role="navigation">--}%
%{--            <ul>--}%
%{--                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--                <li><g:link class="create" action="create"><g:message code="default.new.label" args="[entityName]" /></g:link></li>--}%
%{--            </ul>--}%
%{--        </div>--}%
<div class="container centered">

    <br><h1 class="title">Usuarios</h1>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">${flash.message}</div>
    </g:if>

%{--        <div id="spinner">--}%
%{--            <strong>Loading...</strong>--}%
%{--            <div class="spinner-grow" role="status"></div>--}%
%{--        </div>--}%

    <div class="input-group" id="buttons">
        <div class="md-1">
            <button class="btn btn-success"><g:link class="link-light" action="create"><g:message
                    code="default.new.label" args="[entityName]"/></g:link></button>
        </div>
    </div>
    <br>
    <div class="input-group" id="buttons">
        <div class="col-4">
            <label for="query" class="form-label">Buscar</label>
            <div id="custom-search-input" style="">
                <div class="input-group">

                    <g:form action="index" id="${page}"  class="d-flex align-items-center" method="POST">
                        <g:field type="text" class="form-control" value="${query?.name}" name="name"></g:field>
                        <button class="btn btn-outline-dark" type="submit">🔍</button>
                    </g:form>                </div>
            </div>
        </div>
    </div>

    <div class="card-container">
        <g:each in="${users}" status="i" var="user">
            <div class="card text-white bg-dark mb-3 card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-8 d-flex justify-content-center">
                            <h3 class="card-title text-capitalize">${user?.userName}</h3>
                        </div>
                        <g:form resource="${this.user}" method="DELETE" action="delete" id="${user?.id}"
                                class="col-4 d-flex justify-content-between">
                            <fieldset class="col">
                                <div class="col d-flex justify-content-between">
                                    <g:link class="btn btn-warning" action="edit" resource="${this.user}"
                                            id="${user?.id}">🔧</g:link>
                                    <input class="btn btn-danger" type="submit" value="❌"
                                           onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');"/>
                                </div>
                            </fieldset>
                        </g:form>
                    </div>
                </div>

                <div class="card-body">
                    <h5 class="card-title">Datos Personales:</h5>

                    <div class="row">
                        <div class="col">
                            <p class="card-text">Nombre: <span
                                    class="text-capitalize">${user?.personalData?.name}</span></p>

                            <p class="card-text">Apellido: <span
                                    class="text-capitalize">${user?.personalData?.lastName}</span></p>

                            <p class="card-text">Documento: <span
                                    class="text-capitalize">${user?.personalData?.document}</span></p>

                            <p class="card-text">R.U.C: <span class="text-capitalize">${user?.personalData?.ruc}</span>
                            </p>
                        </div>

                        <div class="col">
                            <p class="card-text">Dirección: <span
                                    class="text-capitalize">${user?.personalData?.address}</span></p>

                            <p class="card-text">Nacimiento: <span
                                    class="text-capitalize">${user?.personalData?.dob}</span></p>

                            <p class="card-text">Tel: <span class="text-capitalize">${user?.personalData?.phone}</span>
                            </p>

                            <p class="card-text">Email: <span>${user?.personalData?.email}</span></p>
                        </div>
                    </div>

                    <h5 class="card-title">Roles:</h5>

                    <div class="row-container">
                        <g:each in="${user?.roles}" status="j" var="role">
                            <p class="card-text">${role?.name}</p>
                        </g:each>
                    </div>
                </div>
            </div>
        </g:each>

    </div>

    <nav aria-label="...">
        <ul class="pagination" id="pagination">
            <g:each in="${pagination}" status="i" var="pageNum">
                <g:if test="${pageNum == page}">
                    <li class="page-item active"><g:link class="page-link" action="index"
                                                         id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
                <g:if test="${pageNum != page}">
                    <li class="page-item"><g:link class="page-link" action="index"
                                                  id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
            </g:each>

        </ul>
    </nav>
</div>

</body>
</html>