<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'user.label', default: 'User')}"/>
    <title>Crear :: Healthpotions</title></head>

<body>
%{--        <div class="nav" role="navigation">--}%
%{--            <ul>--}%
%{--                <li><a class="home" href="${createLink(uri: '/')}"><g:message code="default.home.label"/></a></li>--}%
%{--                <li><g:link class="list" action="index"><g:message code="default.list.label" args="[entityName]" /></g:link></li>--}%
%{--            </ul>--}%
%{--        </div>--}%
<div id="create-user" class="container" role="main">
    <h1>Crear Usuario</h1>
    <g:if test="${flash.message}">
        <div class="message" role="status">${flash.message}</div>
    </g:if>
    <g:hasErrors bean="${this.user}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.user}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message
                        error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <g:form action="save">
        <div class="row">
            <div class="col col-lg-4">
                <h4>Datos de Cuenta</h4>
                <fieldset class="form">
                    <g:render template="form"/>
                </fieldset>
                <fieldset class="form">
                    <div class="container-fluid col-12">
                        <div class="row g-3 align-items-center">
                            <div class="row g-2 align-items-center">
                                <div class="col-4">
                                    <label for="password" class="col-form-label">Contraseña *</label>
                                </div>

                                <div class="col-8">
                                    <g:field type="password" name="password" maxlength="30" required="true"
                                             class="form-control"
                                             value="${userBean?.password}" placeholder="Aa0000000*"/>
                                </div>
                            </div>

                            <div class="row g-2 align-items-center">
                                <div class="col-4">
                                    <label for="password2" class="col-form-label">Repetir Contraseña *</label>
                                </div>

                                <div class="col-8">
                                    <g:field type="password" name="password2" maxlength="30" required="true"
                                             class="form-control"
                                             value="${userBean?.password}" placeholder="Aa0000000*"/>
                                </div>
                            </div>

                            <div class="col-10">
                                <span id="passwordHelpInline" class="form-text">
                                    debe tener de 8 a 30 characteres y contener masyusculas, minusculas, numeros y caracteres especiales.
                                </span>
                            </div>
                        </div>
                    </div>
                </fieldset>
            </div>

            <br>

            <div class="col col-lg-8">
                <h4>Datos Personales</h4>
                <fieldset class="form">
                    <g:render template="/personalData/form"/>
                </fieldset>
            </div>
            <br>
        </div>
        <br>

        <div class="row col-6 mx-auto">
            <h4>Roles</h4>
            <div style="display: flex; justify-content: space-around">
                <g:each in="${roles}" status="i" var="role">
                    <div class="d-flex flex-column align-items-center">
                        <div>
                            <span class="badge rounded-pill bg-dark">${role?.name}</span>
                        </div>

                        <div class="form-check form-switch">
                            <g:checkBox name="roles" value="${role?.id}" class="form-check-input" checked="false"/>
                        </div>
                    </div>
                </g:each>
            </div>

        </div>
        <br>
        <div class="container-fluid col-2">
            <fieldset class="buttons">
                <g:link action="index" class="btn-outline-dark btn">Atras</g:link>
                <g:submitButton name="create" class="btn-outline-success btn"
                                value="${message(code: 'default.button.create.label', default: 'Create')}"/>
            </fieldset>
        </div>
    </g:form>

</div>
</body>
</html>
