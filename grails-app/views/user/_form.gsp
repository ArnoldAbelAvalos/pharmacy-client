<div class="container-fluid col-12">
    <div>
        <label for="userName" class="form-label">Nombre de Usuario *</label>

        <div class="input-group mb-3">
            <span class="input-group-text" id="basic-addon1">@</span>
            <g:textField name="userName" maxlength="30" required="true" class="form-control"
                         value="${userBean?.userName}"
                         placeholder="Jhon_00"/>
        </div>
    </div>
</div>
