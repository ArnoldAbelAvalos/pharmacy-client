<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'purchaseInvoice.label', default: 'Factura de Compra')}"/>
    <title>Lista :: Healthpotions</title>
</head>

<body>
<div class="container centered">

    <br><h1 class="title">Facturas de Compra</h1>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">${flash.message}</div>
    </g:if>

    <div class="input-group" id="buttons">
        <div class="md-1">
            <button class="btn btn-success"><g:link class="link-light" action="create"><g:message
                    code="default.new.label" args="[entityName]"/></g:link></button>
        </div>
    </div>
    <br>
    <div class="card-container">
        <g:each in="${purchaseInvoices}" status="i" var="purchaseInvoice">
            <div class="card bg-light mb-3 card">
                <div class="card-header">
                    <div class="row">
                        <div class="col-10 d-flex justify-content-around">
                            <h3 class="card-title text-capitalize">Numero: ${purchaseInvoice?.number}</h3>

                            <h3 class="card-title text-capitalize">Timbrado: ${purchaseInvoice?.stamped}</h3>
                        </div>
                        <g:form resource="${this.purchaseInvoice}" method="DELETE" action="delete" id="${purchaseInvoice?.id}"
                                class="col-2 d-flex justify-content-between">
                            <fieldset class="col">
                                <div class="col d-flex justify-content-between">
                                    <g:link class="btn btn-sm btn-secondary" action="show" resource="${this.purchaseInvoice}"
                                            id="${purchaseInvoice?.id}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
                                        <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                                    </svg>
                                    </g:link>
                                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Estas seguro?')}');">
                                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                                        </svg>
                                    </button>
                                </div>
                            </fieldset>
                        </g:form>
                    </div>
                </div>

                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <p class="card-text">Proveedor: <span
                                    class="text-capitalize">${purchaseInvoice?.supplier.personalData.name}</span></p>

                            <p class="card-text">Condición: <span
                                    class="text-capitalize">${purchaseInvoice?.condition}</span></p>

                            <p class="card-text">Metodo de Pago: <span
                                    class="text-capitalize">${purchaseInvoice?.paymentMethod}</span></p>

                            </p>
                        </div>

                        <div class="col">
                            <%@ page import="java.time.format.DateTimeFormatter" %>
                            <p class="card-text">Fecha: <span
                                    class="text-capitalize">${purchaseInvoice?.date.toLocalDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy"))}</span></p>

                            <p class="card-text">Hora : <span
                                    class="text-capitalize">${purchaseInvoice?.date.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))}</span></p>

                        </div>
                    </div>

                    <h5 class="card-title">Total: ${purchaseInvoice?.total}</h5>

                    <div class="row-container">
                        <g:each in="${user?.roles}" status="j" var="role">
                            <p class="card-text">${role?.name}</p>
                        </g:each>
                    </div>
                </div>
            </div>
        </g:each>

    </div>

    <nav aria-label="...">
        <ul class="pagination" id="pagination">
            <g:each in="${pagination}" status="i" var="pageNum">
                <g:if test="${pageNum == page}">
                    <li class="page-item active"><g:link class="page-link" action="index"
                                                         id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
                <g:if test="${pageNum != page}">
                    <li class="page-item"><g:link class="page-link" action="index"
                                                  id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
            </g:each>

        </ul>
    </nav>
</div>
</body>
</html>