<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'purchaseInvoice.label', default: 'Factura de Compra')}" />
        <title>Ver :: Healthpotions</title>
    </head>
    <body>
    <div id="show-saleInvoice" class="content scaffold-show" role="main">
        <div style="padding-top: 2%"></div>
        <div class="container-fluid col-9" >
            <div class="row">
                <div class="col-md">
                    <h2 class="align-content-center" style="padding-top: 5%">Factura de Compra</h2>
                </div>
                <div class="col-md">
                    <div class="form-floating master">
                        <g:textField readOnly="true" name="stamping" maxlength="50" value="${purchaseInvoice?.stamped}" class="form-control "/>
                        <label for="stamping">Timbrado</label>
                    </div>
                </div>
                <div class="col-md">
                    <div class="form-floating master">
                        <g:field readOnly="true" type="number" min="0" name="number" maxlength="50" value="${purchaseInvoice?.number}" class="form-control"/>
                        <label for="number">Número</label>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-fluid col-9" style="padding-top: 2%"><hr></div>

        <div class="container-fluid col-9 bg-light card-body" style="padding-top: 2%">
            <div class="receipt">
                <div class="form-group row">
                    <div class="col-8 master">
                        <%@ page import="java.time.format.DateTimeFormatter" %>
                        <span class="badge bg-success">${purchaseInvoice?.status}</span><br>
                        <label><strong>Fecha:</strong> ${purchaseInvoice?.date.toLocalDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy"))}</label><br>
                        <label><strong>Hora:</strong> ${purchaseInvoice?.date.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))}</label><br>
                        <label><strong>Método de pago:</strong> ${purchaseInvoice?.paymentMethod}</label><br>
                        <label><strong>Condición:</strong> ${purchaseInvoice?.condition}</label>
                    </div>
                </div>
                <br>
                <div class="form-group master ${hasErrors(bean: purchaseInvoice, field: 'client', 'error')} required">
                    <label class="fw-bold" for="client">Proveedor</label>
                    <g:textField  name="client"  value="${purchaseInvoice?.supplier.personalData.name}" class="form-control" readOnly="true"/>
                </div>
                <br>
                <div class="card" >
                    <div class="card-header bg-secondary bg-opacity-25">
                        <h5 class="card-title">Producto(s)</h5>
                    </div>
                    <div class="card-body" name="body">
                        <div class="table-responsive">
                            <table id="detailsTable" class="table">
                                <thead>
                                <tr>
                                    <th>CANTIDAD</th>
                                    <th>DESCRIPCION</th>
                                    <th>PRECIO UNITARIO</th>
                                    <th>TOTAL</th>
                                </tr>
                                </thead>
                                <tbody>
                                <g:each in="${purchaseInvoice?.details}" var="detail">
                                    <tr class="data">
                                        <td>${detail?.quantity}</td>
                                        <td>${detail?.product.name} - ${detail?.product.barcode}</td>
                                        <td>${detail?.unitPrice}</td>
                                        <td>${detail?.total}</td>
                                    </tr>
                                </g:each>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <td colspan="1"></td>
                                    <td class="fw-bold" colspan="2">TOTAL</td>
                                    <td class="fw-bold">${purchaseInvoice?.total}</td>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <div class="container-fluid col-9" align="right">
            <div class="row">
                <hr>
                <fieldset class="buttons">
                    <a href="${createLink(uri: '/purchaseInvoice/index')}" class="btn btn-outline-dark"><g:message code="default.link.skip.label" default="Atrás"/></a>
                </fieldset>
            </div>
        </div>
    </div>
    </body>
    </body>
</html>
