<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'purchaseInvoice.label', default: 'Factura de Compra')}"/>
    <title>Crear :: Healthpotions</title>
</head>

<body>

<div style="padding-top: 2%;" id="spinner">
    <div class="d-flex justify-content-center">
        <strong>Loading...</strong>
        <div class="spinner-grow" role="status"></div>
    </div>

</div>
<div style="padding-top: 2%" class="container" hidden id="form">

    <div class="container-fluid col-9">
        <div class="row">
            <div class="col-md">
                <div class="form-floating master">
                    <input type="text" maxlength="50" value="${stamped}" class="form-control" id="stamped">
                    <label for="stamped">Timbrado</label>
                </div>
            </div>

            <div class="col-md">
                <div class="form-floating master">
                    <input type="number" min="0" maxlength="50" value="" class="form-control" id="number">
                    <label for="number">Número</label>
                </div>
            </div>
        </div>
    </div>

    <div class="container-fluid col-9" style="padding-top: 2%"><hr></div>

    <div class="container-fluid col-9 bg-light card-body" style="padding-top: 2%">
        <div class="receipt">
            <div class="form-group row">
                <div class="col-4 master">
                    <label for="date" class="col-6 col-form-label fw-bold">FECHA</label>
                    <input type="date" class="form-control" id="date" value="${date}" disabled="true">
                </div>

                <div class="col-4 master">
                    <label for="paymentMethod" class="col-6 col-form-label fw-bold">MÉTODO DE PAGO</label>
                    <select id="paymentMethod" name="paymentMethod" class="form-select">
                        <option value="Efectivo" selected>Efectivo</option>
                        <option value="Tarjeta Debito">Tarjeta Debito</option>
                        <option value="Tarjeta Credito">Tarjeta Credito</option>
                        <option value="Transferencia">Transferencia</option>
                        <option value="Cheque">Cheque</option>
                    </select>
                </div>

                <div class="col-4">
                    <label class="col-6 col-form-label fw-bold">COND. DE VENTA</label>
                    <br>

                    <div class="form-check form-check-inline master">
                        <input name="condition" class="form-check-input" value="Contado" type="radio" id="contado_check"
                               checked>
                        <label class="form-check-label" for="contado_check">Contado</label>
                    </div>

                    <div class="form-check form-check-inline master">
                        <input name="condition" class="form-check-input" value="Credito" type="radio"
                               id="credito_check">
                        <label class="form-check-label" for="credito_check">Crédito</label>
                    </div>
                </div>
            </div>
            <br>

            <div class="form-group master ${hasErrors(bean: saleInstance, field: 'client', 'error')} required">
                <label class="fw-bold" for="supplier">Proveedor<span class="required-indicator">*</span></label>
                <select id="suppliers" class="form-select">
                </select>
            </div>

            <br><hr>

            <div class="form-group row">
                <div class="col-2 master">
                    <label for="quantity" class="col-6 col-form-label fw-bold">CANTIDAD</label>
                </div>

                <div class="col-8 master">
                    <label for="product" class="col-6 col-form-label fw-bold">PRODUCTO</label>
                </div>

            </div>

            <div class="form-group row">
                <div class="col-2 master">
                    <input type="number" id="quantity" min="1" class="form-control" value="1"/>
                </div>

                <div class="col-8 master">
                    <select id="products" class="form-select">

                    </select>

                </div>

                <div class="col-2 master">
                    <button id="addDetailBtn" class="btn btn-outline-success">Agregar</button>
                </div>
            </div>

            <br>

            <div class="card">
                <div class="card-header bg-secondary bg-opacity-25">
                    <h5 class="card-title">Producto(s)</h5>
                </div>

                <div class="card-body" name="body">
                    <div class="table-responsive">
                        <table id="detailsTable" class="table">
                            <thead>
                            <tr>
                                <th>CANTIDAD</th>
                                <th>DESCRIPCION</th>
                                <th>PRECIO UNITARIO</th>
                                <th>TOTAL</th>
                                <th></th>
                            </tr>
                            </thead>
                            <tbody id="details">
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <br>

            <div class="form-group master d-flex justify-content-end gap-2 container">
                <h5 for="total">TOTAL</h5>

                <div id="total">

                </div>
            </div>
        </div>
        <br>

        <div class="modal-footer">
            <a href="index" class="btn btn-outline-dark" tabindex="-1"><g:message code="default.link.skip.label"
                                                                                  default="Volver"/></a>
            <button id="submitBtn" class="btn btn-outline-success">Crear</button>
        </div>
    </div>
</div>
<asset:javascript src="purchase-invoice.js" type="module"/>
</body>
</html>
