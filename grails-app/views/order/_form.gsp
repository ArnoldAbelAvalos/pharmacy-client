%{--<div style="padding-top: 2%"></div>--}%
<div class="container-fluid col-9" >
	<div class="row">
		<div class="col-md">
			<h4 class="align-content-center" style="padding-top: 5%">Órdenes de Pedidos</h4>
		</div>
	</div>
</div>
<div class="container-fluid col-9" style="padding-top: 2%"><hr></div>
<div class="container-fluid col-9 bg-light card-body" style="padding-top: 2%">
	<div class="receipt">
		<div class="form-group row">
			<div class="col-4 master">
				<label class="col-6 col-form-label fw-bold">FECHA</label>
				<g:field name="date" type="date" value="${date}" class="form-control" readOnly="true"/>
			</div>
		</div>
		<br>
		<div class="form-group master ${hasErrors(bean: order, field: 'order', 'error')} required">
			<label class="fw-bold" for="supplier">NOMBRE Y/O RAZÓN SOCIAL<span class="required-indicator">*</span></label>
			<g:select id="supplier" name="supplier" from="${suppliers}" optionKey="id" optionValue="${{it.personalData.name + ' - '+ it.personalData.ruc}}" value="" class="form-select"/>
		</div>

		<br><hr>

		<div class="form-group row">
			<div class="col-2 master">
				<label for="quantity" class="col-6 col-form-label fw-bold">CANTIDAD</label>
			</div>
			<div class="col-6 master">
				<label for="invoiceDate" class="col-6 col-form-label fw-bold">PRODUCTO</label>
			</div>
			<div class="col-1 master">
			</div>
		</div>

		<div class="form-group row">
			<div class="col-2 master">
%{--				<input type="number" id="quantity" min="1" name="quantity" class="form-control" />--}%
				<g:field name="quantity" value="" type="number" min="0" class="form-control"/>
			</div>
			<div class="col-6 master">
%{--				<input type="text" id="productName" name="productName" class="form-control" />--}%
%{--				<g:textField  name="product"  value="" class="form-control"/>--}%
				<g:select id="product" name="product" from="${products}" optionKey="id" optionValue="name" value="" class="form-select"/>

			</div>

			<div class="col-1 master">
				<a id="addToList" class="btn btn-outline-success">Agregar</a>
			</div>
		</div>

		<br>
		<div class="card" >
			<div class="card-header bg-secondary bg-opacity-25">
				<h5 class="card-title">Producto(s)</h5>
			</div>
			<div class="card-body" name="body">
				<div class="table-responsive">
					<table id="detailsTable" class="table">
						<thead>
						<tr>
							<th>CANTIDAD</th>
							<th>DESCRIPCION</th>
							<th></th>
						</tr>
						</thead>
						<tbody id="details">
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<br>
	</div>
	<br>
	<div class="modal-footer">
		<g:link action="index" class="btn-outline-dark btn">Atras</g:link>
		<g:submitButton id = "order" name="create" class="btn btn-outline-success" value="${message(code: 'default.button.create.label', default: 'Crear')}" />
	</div>
</div>

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<g:javascript>

	let list = []
	var url="http://localhost:8080/api/"

	fetch(url+'products')
			.then(response => response.json())
			.then(data => {
		productsData = data
	})

	$("#addToList").click(function (e) {
		e.preventDefault();
        let item = {quantity: parseInt($("#quantity").val()), product: parseInt($("#product").val()), order: 0}
		if(!isNaN(item.product )) {
			list.push(item)
			fillDetails(list)
			clearItem();
		}
	});

	function fillDetails(items){
		total = list.reduce((prev,curr)=> prev + curr.total, 0)
        let parent = $("#details");
		parent.html("")
        items.forEach(item => {
			let name = productsData.content.filter(product => product.id == item.product)[0].name
			let productItem = '<tr><td>'+item.quantity+'</td><td>' + name + '</td><td><a id="'+item.product +'" href="#" class="deleteItem">Eliminar</a></td></tr>';
			parent.append(productItem)
        })

	}


	//After Add A New Order In The List, Clear Clean The Form For Add More Order.
	function clearItem() {
		$("#product").val('');
		$("#quantity").val('');
	}
	// After Add A New Order In The List, If You Want, You Can Remove It.
	$(document).on('click', 'a.deleteItem', function (e) {
		e.preventDefault();
        console.log(e.target.id)
		list = list.filter(item => item.product != e.target.id)
		fillDetails(list)
	});

	function order(data) {
		return $.ajax({
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			type: 'POST',
			url: url + '/orders/details',
			data: data,
			success: function (result) {
				location = "/order/index";
				// location.reload()
			},
			error: function () {
				alert("Error!")
			}
		});
	}


	$("#order").click(function (e) {
		e.preventDefault();

		var data = JSON.stringify({
			supplier: $("#supplier").val(),
			status: "Creado",
			details: list
		});


		$.when(order(data)).then(function (response) {
			console.log(response);
		}).fail(function (err) {
			console.log(err);
            console.log(data);
		});
	});

</g:javascript>
