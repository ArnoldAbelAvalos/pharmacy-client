<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'order.label', default: 'Order')}" />
    <title>Ver :: Healthpotions</title>
</head>
<body>
<div id="show-saleInvoice" class="content scaffold-show" role="main">
    <div style="padding-top: 2%"></div>
    <div class="container-fluid col-9" >
        <div class="row">
            <div class="col-md">
                <h2 class="align-content-center" style="padding-top: 5%">Orden de Pedido #${order?.id}</h2>
            </div>
        </div>
    </div>
    <div class="container-fluid col-9" style="padding-top: 2%"><hr></div>

    <div class="container-fluid col-9 bg-light card-body" style="padding-top: 2%">
        <div class="receipt">
            <div class="form-group row">
                <div class="col-8 master">
                    <span class="badge bg-success">${order?.status}</span><br>
                    <%@ page import="java.time.format.DateTimeFormatter" %>
                    <label><strong>Fecha:</strong> ${order.date.toLocalDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy"))}</label><br>
                    <label><strong>Hora:</strong> ${order.date.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))}</label><br>

                </div>
            </div>
            <br>
            <div class="form-group master">
                <label class="fw-bold" for="supplier">PROVEEDOR</label>
                <g:textField  name="supplier"  value="${order?.supplier.personalData.name}" class="form-control" readOnly="true"/>
            </div>
            <br>
            <div class="card" >
                <div class="card-header bg-secondary bg-opacity-25">
                    <h5 class="card-title">Producto(s)</h5>
                </div>
                <div class="card-body" name="body">
                    <div class="table-responsive">
                        <table id="detailsTable" class="table">
                            <thead>
                            <tr>
                                <th>CANTIDAD</th>
                                <th>DESCRIPCION</th>
                            </tr>
                            </thead>
                            <tbody>
                            <g:each in="${order?.details}" var="detailsInstance">
                                <tr class="data">
                                    <td>${detailsInstance?.quantity}</td>
                                    <td>${detailsInstance?.product.name} - ${detailsInstance?.product.barcode}</td>
                                </tr>
                            </g:each>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <br>
    <div class="container-fluid col-9" align="right">
        <div class="row">
            <hr>
            <fieldset class="buttons">
                <a href="${createLink(uri: '/order/index')}" class="btn btn-outline-dark"><g:message code="default.link.skip.label" default="Atrás"/></a>
            </fieldset>
        </div>
    </div>
</div>
</body>
</html>