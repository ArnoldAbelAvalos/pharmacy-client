<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'order.label', default: 'Order')}" />
    <title>Crear :: Healthpotions</title>
</head>

<body>

<div id="create-type" class="content scaffold-create" role="main">
    <g:hasErrors bean="${this.order}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.order}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form action="save" >
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <br>
    %{--        <div class="container-fluid col-9" align="right">--}%
    %{--            <div class="row">--}%
    %{--                <hr>--}%
    %{--                <fieldset class="buttons">--}%
    %{--                    <a href="index" class="skip" tabindex="-1"><g:message code="default.link.skip.label" default="Volver"/></a>--}%
    %{--                    <g:submitButton id = "save" name="create" class="save btn-primary btn-sm" value="${message(code: 'default.button.create.label', default: 'Create')}" />--}%
    %{--                </fieldset>--}%
    %{--            </div>--}%
    %{--        </div>--}%
    </g:form>
</body>
</html>
