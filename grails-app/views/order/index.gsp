<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'order.label', default: 'Pedidos')}"/>
    <title>Lista :: Healthpotions</title>
</head>

<body>
<div class="container centered">

    <br><h1 class="title">Ordenes de Pedidos</h1>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">${flash.message}</div>
    </g:if>

    <div class="input-group" id="buttons">
        <div class="md-1">
            <button class="btn btn-success"><g:link class="link-light" action="create"><g:message
                    code="default.new.label" args="[entityName]"/></g:link></button>
        </div>
    </div>
    <br>

    <table class="table" id="table">
        <thead>
        <tr>
            <th scope="col">Fecha</th>
            <th scope="col">Hora</th>
            <th scope="col">Proveedor</th>
            <th scope="col">Cant. Productos</th>
            <th scope="col">Estado</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${orders}" status="i" var="order">
            <tr>
                <%@ page import="java.time.format.DateTimeFormatter" %>
                <td>${order.date.toLocalDate().format(DateTimeFormatter.ofPattern("dd-MMM-yy"))}</td>
                <td>${order.date.toLocalTime().format(DateTimeFormatter.ofPattern("HH:mm:ss"))}</td>
                <td>${order?.supplier.personalData.name}</td>
                <td>${order?.details.size()}</td>
                <g:if test="${order.status == "Creado"}">
                    <td><span class="badge bg-success text-light">${order.status}</span></td>
                </g:if>
                <g:elseif test="${order.status == "Anulado"}">
                    <td><span class="badge bg-danger text-light">$order.status}</span></td>
                </g:elseif>
                <g:else>
                    <td><span class="badge bg-dark text-light">${order.status}</span></td>
                </g:else>
                <td><g:form resource="${this.order}" method="DELETE" action="delete" id="${order?.id}"
                            class="justify-content-between">
                    <g:link class="btn btn-sm btn-secondary" action="show" resource="${this.order}"
                            id="${order?.id}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                    </svg>
                    </g:link>
                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Estas seguro?')}');">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </button>
                </g:form>
                </td>
            </tr>

        </g:each>

        </tbody>
    </table>

    <nav aria-label="...">
        <ul class="pagination" id="pagination">
            <g:each in="${pagination}" status="i" var="pageNum">
                <g:if test="${pageNum == page}">
                    <li class="page-item active"><g:link class="page-link" action="index"
                                                         id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
                <g:if test="${pageNum != page}">
                    <li class="page-item"><g:link class="page-link" action="index"
                                                  id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
            </g:each>

        </ul>
    </nav>
</body>
</html>
