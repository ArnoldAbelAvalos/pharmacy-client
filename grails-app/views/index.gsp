<!doctype html>
<html>
<head>
    <meta name="layout" content="main"/>
</head>

<body>

    <div>
        <img src="https://www.consalud.es/uploads/s1/15/51/43/7/imagen-de-recurso-de-farmacia-y-medicamentos-foto-i-viewfinder-rapisan-swangphon-john-micof.jpeg" width="100%" height="10%">
    </div>
    <br>

    <div class="container text-center">
        <h1 class="title">Bienvenido</h1>
    </div>

    <div class="section">
        <div class="container">
            <div class="row">
                <div class="col-md-12 text-center">
                    <h5>Acercate a una sucursal y registrate gratis para disfrutar de descuentos exclusivos, sorteos, promos diarias y ¡mucho más!</h5>
                </div>
            </div>
        </div>
    </div>

    <hr><br>

    <div class="section section-contact-us text-center">
        <div class="container">
            <h3 class="text-muted">Tenes una duda o sugerencia?</h3>
            <h3 class="title">Comunicate con nosotros!</h3>
        </div>
    </div>

    <div class="container">
        <fieldset class="form">
            <g:render template="contact/form"/>
        </fieldset>
    </div>
<br>
    </body>
</html>



