<html>
<head>
    <meta name="layout" content="main"/>
<g:set var="entityName" value="${message(code: 'category.label', default: 'Categoria')}"/>
<title>Lista :: Healthpotions</title>
<asset:stylesheet src="index.css"/>
</head>
<body>
<div class="container centered">
<br><h1 class="title">Categorias</h1>

<g:if test="${flash.message}">
    <div class="alert alert-success" role="status">${flash.message}</div>
</g:if>

<div class="container-fluid buttons" id="buttons">
    <div class="md-1">
        <button class="btn btn-success"><g:link class="link-light" action="create"><g:message
                code="default.new.label" args="[entityName]"/></g:link></button>
    </div>

    <div class="md-1 d-flex align-items-center">
        <label for="query" class="form-label">Buscar:</label>
        <g:form action="index" id="${page}"  class="d-flex align-items-center" method="POST">
            <g:field type="text" class="form-control" value="${query?.name}" name="name"></g:field>
            <button class="btn btn-outline-dark" type="submit">🔍</button>
        </g:form>
    </div>
</div>
<table class="table" id="table">
    <thead>
    <tr>
        <th scope="col">Nombre</th>
        <th scope="col"></th>
    </tr>
    </thead>
    <tbody>
        <g:each in="${categories}" status="i" var="categoryInstance">
            <tr>
                <td>${fieldValue(bean: categoryInstance, field: "name")}</td>
                <td><g:form resource="${this.categoryInstance}" method="DELETE" action="delete" id="${categoryInstance?.id}"
                            class="justify-content-between">
                    <g:link class="btn btn-sm btn-secondary" action="show" resource="${this.categoryInstance}"
                            id="${categoryInstance?.id}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
                        <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                    </svg>
                    </g:link>
                    <g:link class="btn btn-sm btn-primary" action="edit" resource="${this.categoryInstance}"
                            id="${categoryInstance?.id}">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-pencil-square" viewBox="0 0 16 16">
                            <path d="M15.502 1.94a.5.5 0 0 1 0 .706L14.459 3.69l-2-2L13.502.646a.5.5 0 0 1 .707 0l1.293 1.293zm-1.75 2.456-2-2L4.939 9.21a.5.5 0 0 0-.121.196l-.805 2.414a.25.25 0 0 0 .316.316l2.414-.805a.5.5 0 0 0 .196-.12l6.813-6.814z"/>
                            <path fill-rule="evenodd" d="M1 13.5A1.5 1.5 0 0 0 2.5 15h11a1.5 1.5 0 0 0 1.5-1.5v-6a.5.5 0 0 0-1 0v6a.5.5 0 0 1-.5.5h-11a.5.5 0 0 1-.5-.5v-11a.5.5 0 0 1 .5-.5H9a.5.5 0 0 0 0-1H2.5A1.5 1.5 0 0 0 1 2.5v11z"/>
                        </svg>
                    </g:link>
                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Estas seguro?')}');">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </button>
                </g:form>
                </td>
            </tr>

        </g:each>
    </tbody>
</table>

    <nav aria-label="...">
        <ul class="pagination" id="pagination">
            <g:each in="${pagination}" status="i" var="pageNum">
                <g:if test="${pageNum == page}">
                    <li class="page-item active"><g:link class="page-link" action="index"
                                                         id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
                <g:if test="${pageNum != page}">
                    <li class="page-item"><g:link class="page-link" action="index"
                                                  id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
            </g:each>

        </ul>
    </nav>
</div>
</body>
</html>