<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
    <title>Ver :: Healthpotions</title>
</head>

<body>

<div id="create-type" class="content scaffold-create" role="main">
    <g:hasErrors bean="${this.category}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.category}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <div class="container-fluid col-6">
        <div class="row">
            <h4 class="align-content-center" style="padding-top: 5%">Categoria</h4>
            <hr>
            <g:if test="${flash.message}">
                <div class="alert alert-success" role="status">
                    La categoria ha sido creada!
                </div>
            </g:if>
            <div class="form-group row" style="padding-bottom: 10px; padding-left: 0px">
                <div class="col-1">
                    <g:form resource="${this.category}" method="PUT" action="edit" id="${categoryInstance?.id}">
                        <g:submitButton action="edit" class="btn btn-sm btn-outline-secondary" name="submit" value="${message(code: 'default.button.edit.label', default: 'Editar')}" />
                    </g:form>
                </div>
                <div class="col-1">
                    <g:form resource="${this.category}" method="DELETE" action="delete" id="${categoryInstance?.id}">
                        <g:submitButton name="submit" class="btn btn-sm btn-outline-danger" value="${message(code: 'default.button.delete.label', default: 'Eliminar')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </g:form>
                </div>
            </div>

            <div class="col-md-6 bg-light card-body">
                <div><label for="name" class="form-label">Nombre</label>
                    <div>
                        <g:textField name="name" maxlength="50" readonly="true" class="form-control" value="${categoryInstance?.name}"/>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="container-fluid col-6" align="right">
        <div class="row">
            <hr>
            <fieldset class="buttons">
                <g:link action="index" class="btn-outline-dark btn">Atras</g:link>
            </fieldset>
        </div>
    </div>

</body>
</html>
