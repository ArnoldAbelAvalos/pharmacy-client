<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
    <title>Editar :: Healthpotions</title>
</head>

<body>

<div id="create-type" class="content scaffold-create" role="main">
    <g:hasErrors bean="${this.category}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.category}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>
    <g:form resource="${this.category}" action="update" method="PUT">
        <g:hiddenField name="id" value="${this.categoryInstance?.id}" />
        <fieldset class="form">
            <g:render template="form"/>
        </fieldset>
        <br>
        <div class="container-fluid col-6" align="right">
            <fieldset class="buttons">
                <a href="${createLink(uri: '/category/index')}" class="btn btn-outline-dark"><g:message code="default.link.skip.label" default="Atras"/></a>
                <g:submitButton name="create" class="btn btn-outline-success" value="${message(code: 'default.button.update.label', default: 'Actualizar')}" />
            </fieldset>
        </div>
    </g:form>
</body>
</html>
