<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'category.label', default: 'Category')}" />
        <title>Crear :: Healthpotions</title>
    </head>
    <body>
    <div id="create-tcategory" class="content scaffold-create" role="main">
        <g:hasErrors bean="${this.category}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.category}" var="error">
                    <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
        </g:hasErrors>
        <g:form action="save" >
            <fieldset class="form">
                <g:render template="form"/>
            </fieldset>
            <br>
            <div class="container-fluid col-6"  align="right">
                <fieldset class="buttons">
                    <g:link action="index" class="btn-outline-dark btn">Atras</g:link>
                    <g:submitButton name="create" class="btn-outline-success btn"
                                    value="${message(code: 'default.button.create.label', default: 'Create')}"/>
                </fieldset>
            </div>
        </g:form>
    </body>
</html>
