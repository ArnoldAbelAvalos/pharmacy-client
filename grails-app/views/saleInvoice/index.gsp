<!DOCTYPE html>
<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'saleInvoice.label', default: 'Factura de Venta')}"/>
    <title>Lista :: Healthpotions</title>
</head>

<body>
<div class="container centered">

    <br><h1 class="title">Facturas de Ventas</h1>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">${flash.message}</div>
    </g:if>

    <div class="input-group" id="buttons">
        <div class="md-1">
            <button class="btn btn-success"><g:link class="link-light" action="create"><g:message
                    code="default.new.label" args="[entityName]"/></g:link></button>
        </div>

        <div class="md-1 d-flex align-items-center">
            <label for="query" class="form-label">Buscar:</label>
            <g:form action="index" id="${page}"  class="d-flex align-items-center" method="POST">
                <g:field type="text" name="number" class="form-control" value="${query?.number}" ></g:field>
                <button class="btn btn-outline-dark" type="submit">🔍</button>
            </g:form>
        </div>
    </div>
    <br>

    <table class="table" id="table">
            <thead>
            <tr>
                <th scope="col">Fecha</th>
                <th scope="col">Timbrado</th>
                <th scope="col">Número</th>
                <th scope="col">Cliente</th>
                <th scope="col">Condición</th>
                <th scope="col">Cant. Productos</th>
                <th scope="col">Total</th>
                <th scope="col">Estado</th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <g:each in="${saleInvoices}" status="i" var="saleInvoice">
                <tr>
                    <td>${saleInvoice?.invoiceDate}</td>
                    <td>${saleInvoice?.stamping}</td>
                    <td>${saleInvoice?.number}</td>
                    <td>${saleInvoice?.client.personalData.name} ${saleInvoice?.client.personalData.lastName}</td>
                    <td>${saleInvoice?.condition} - ${saleInvoice?.paymentMethod}</td>
                    <td>${saleInvoice?.saleInvoiceDetails.size()}</td>
                    <td>Gs. ${saleInvoice?.total}</td>
                    <g:if test="${saleInvoice.status == "Creado"}">
                        <td><span class="badge bg-success text-light">${saleInvoice.status}</span></td>
                    </g:if>
                    <g:elseif test="${saleInvoice.status == "Anulado"}">
                        <td><span class="badge bg-danger text-light">${saleInvoice.status}</span></td>
                    </g:elseif>
                    <g:else>
                        <td><span class="badge bg-dark text-light">${saleInvoice.status}</span></td>
                    </g:else>
                    <td><g:form resource="${this.saleInvoice}" method="DELETE" action="delete" id="${saleInvoice?.id}"
                                class="justify-content-between">
                        <g:link class="btn btn-sm btn-secondary" action="show" resource="${this.saleInvoice}"
                                id="${saleInvoice?.id}"><svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-list-ul" viewBox="0 0 16 16">
                            <path fill-rule="evenodd" d="M5 11.5a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm0-4a.5.5 0 0 1 .5-.5h9a.5.5 0 0 1 0 1h-9a.5.5 0 0 1-.5-.5zm-3 1a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2zm0 4a1 1 0 1 0 0-2 1 1 0 0 0 0 2z"/>
                        </svg>
                        </g:link>
                        <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Estas seguro?')}');">
                            <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                                <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                                <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                            </svg>
                        </button>
                    </g:form>
                    </td>
                </tr>

            </g:each>

            </tbody>
    </table>

    <nav aria-label="...">
        <ul class="pagination" id="pagination">
            <g:each in="${pagination}" status="i" var="pageNum">
                <g:if test="${pageNum == page}">
                    <li class="page-item active"><g:link class="page-link" action="index"
                                                         id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
                <g:if test="${pageNum != page}">
                    <li class="page-item"><g:link class="page-link" action="index"
                                                  id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
            </g:each>

        </ul>
    </nav>
</body>
</html>
