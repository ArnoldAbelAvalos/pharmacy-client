<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'saleInvoice.label', default: 'SaleInvoice')}" />
        <title>Ver :: Healthpotions</title>
    </head>
    <body>
        <div id="show-saleInvoice" class="content scaffold-show" role="main">
            <div style="padding-top: 2%"></div>
            <div class="container-fluid col-9" >
                <div class="row">
                    <div class="col-md">
                        <h2 class="align-content-center" style="padding-top: 5%">Factura de Venta</h2>
                    </div>
                    <div class="col-md">
                        <div class="form-floating master">
                            <g:textField readOnly="true" name="stamping" maxlength="50" value="${saleInstance?.stamping}" class="form-control "/>
                            <label for="stamping">Timbrado</label>
                        </div>
                    </div>
                    <div class="col-md">
                        <div class="form-floating master">
                            <g:field readOnly="true" type="number" min="0" name="number" maxlength="50" value="${saleInstance?.number}" class="form-control"/>
                            <label for="number">Número</label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="container-fluid col-9" style="padding-top: 2%"><hr></div>

            <div class="container-fluid col-9 bg-light card-body" style="padding-top: 2%">
                <div class="receipt">
                    <div class="form-group row">
                        <div class="col-8 master">
                            <g:if test="${saleInstance.status == "Creado"}">
                                <span class="badge bg-success text-light">${saleInstance.status}</span>
                            </g:if>
                            <g:elseif test="${saleInstance.status == "Anulado"}">
                                <span class="badge bg-danger text-light">${saleInstance.status}</span>
                            </g:elseif>
                            <g:else>
                                <span class="badge bg-dark text-light">${saleInstance.status}</span>
                            </g:else><br>
                            <label><strong>Fecha:</strong> ${saleInstance?.invoiceDate}</label><br>
                            <label><strong>Método de pago:</strong> ${saleInstance?.paymentMethod}</label><br>
                            <label><strong>Condición:</strong> ${saleInstance?.condition}</label>
                        </div>
                    </div>
                    <br>
                    <div class="form-group master ${hasErrors(bean: saleInstance, field: 'client', 'error')} required">
                        <label class="fw-bold" for="client">NOMBRE Y/O RAZÓN SOCIAL</label>
                        <g:textField  name="client"  value="${saleInstance?.client.personalData.name} ${saleInstance?.client.personalData.lastName}" class="form-control" readOnly="true"/>
                    </div>
                    <br>
                    <div class="card" >
                        <div class="card-header bg-secondary bg-opacity-25">
                            <h5 class="card-title">Producto(s)</h5>
                        </div>
                        <div class="card-body" name="body">
                            <div class="table-responsive">
                                <table id="detailsTable" class="table">
                                    <thead>
                                    <tr>
                                        <th>CANTIDAD</th>
                                        <th>DESCRIPCION</th>
                                        <th>PRECIO UNITARIO</th>
                                        <th>TOTAL</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <g:each in="${saleInstance?.saleInvoiceDetails}" var="detailsInstance">
                                        <tr class="data">
                                            <td>${detailsInstance?.quantity}</td>
                                            <td>${detailsInstance?.product.name} - ${detailsInstance?.product.barcode}</td>
                                            <td>${detailsInstance?.unitPrice}</td>
                                            <td>${detailsInstance?.total}</td>
                                        </tr>
                                    </g:each>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="1"></td>
                                        <td class="fw-bold" colspan="2">TOTAL GS.</td>
                                        <td class="fw-bold">${saleInstance?.total}</td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="container-fluid col-9" align="right">
                <div class="row">
                    <hr>
                    <fieldset class="buttons">
                        <a href="${createLink(uri: '/saleInvoice/index')}" class="btn btn-outline-dark"><g:message code="default.link.skip.label" default="Atrás"/></a>
                    </fieldset>
                </div>
            </div>
        </div>
    </body>
</html>