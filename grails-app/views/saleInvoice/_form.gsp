<div style="padding-top: 2%"></div>
<div class="container-fluid col-9" >
	<div class="row">
		<div class="col-md">
			<h4 class="align-content-center" style="padding-top: 5%">Factura de Venta</h4>
		</div>
		<div class="col-md">
			<div class="form-floating master">
				<g:textField name="stamping" maxlength="50" value="142.758.1541" class="form-control "/>
				<label for="stamping">Timbrado</label>
			</div>
		</div>
		<div class="col-md">
			<div class="form-floating master">
				<g:field type="number" min="0" name="number" maxlength="50" value="" class="form-control"/>
				<label for="number">Número</label>
			</div>
		</div>
	</div>
</div>
<div class="container-fluid col-9" style="padding-top: 2%"><hr></div>
<div class="container-fluid col-9 bg-light card-body" style="padding-top: 2%">
	<div class="receipt">
		<div class="form-group row">
			<div class="col-4 master">
				<label for="invoiceDate" class="col-6 col-form-label fw-bold">FECHA</label>
				<g:field  name="invoiceDate" type="date" value="${date}" class="form-control" readOnly="true"/>
			</div>

			<div class="col-4 master">
				<label for="paymentMethod" class="col-6 col-form-label fw-bold">MÉTODO DE PAGO</label>
				<select id="paymentMethod" name="paymentMethod" class="form-select">
					<option value="Efectivo" selected>Efectivo</option>
					<option value="Tarjeta Debito">Tarjeta Debito</option>
					<option value="Tarjeta Credito">Tarjeta Credito</option>
					<option value="Transferencia">Transferencia</option>
					<option value="Cheque">Cheque</option>
				</select>
			</div>

			<div class="col-4">
				<label class="col-6 col-form-label fw-bold">COND. DE VENTA</label>
				<br>
				<div class="form-check form-check-inline master">
					<input name="condition" class="form-check-input" value="Contado" type="radio" id="contado_check" checked>
					<label class="form-check-label" for="contado_check">Contado</label>
				</div>
				<div class="form-check form-check-inline master">
					<input name="condition" class="form-check-input" value="Credito" type="radio" id="credito_check">
					<label class="form-check-label" for="credito_check">Crédito</label>
				</div>
			</div>
		</div>
		<br>
		<div class="form-group master ${hasErrors(bean: saleInstance, field: 'client', 'error')} required">
			<label class="fw-bold" for="client">NOMBRE Y/O RAZÓN SOCIAL<span class="required-indicator">*</span></label>
			<g:select id="client" name="client" from="${clients}" optionKey="id" optionValue="${{it.personalData.name +' '+it.personalData.lastName+' - '+it.personalData.ruc}}" value="" class="form-select"/>
		</div>

		<br><hr>

		<div class="form-group row">
			<div class="col-2 master">
				<label for="quantity" class="col-6 col-form-label fw-bold">CANTIDAD</label>
			</div>
			<div class="col-6 master">
				<label for="invoiceDate" class="col-6 col-form-label fw-bold">PRODUCTO</label>
			</div>

			<div class="col-3 master">
				<label for="paymentMethod" class="col-6 col-form-label fw-bold">PRECIO</label>
			</div>
			<div class="col-1 master">
			</div>
		</div>

		<div class="form-group row">
			<div class="col-2 master">
%{--				<input type="number" id="quantity" min="1" name="quantity" class="form-control" />--}%
				<g:field name="quantity" value="" type="number" min="1" class="form-control"/>
			</div>
			<div class="col-6 master">
%{--				<input type="text" id="productName" name="productName" class="form-control" />--}%
%{--				<g:textField  name="product"  value="" class="form-control"/>--}%
				<g:select id="product" name="product" from="${products}" optionKey="id" optionValue="${{it.name + ' '+it.barcode + ' - Precio: ' + it.price}}" value="" class="form-select"/>

			</div>

			<div class="col-3 master">
%{--				<input type="number" id="price" name="price" class="form-control" />--}%
				<g:field type="number" value="" min="0" name="unitPrice" class="form-control"/>
			</div>

			<div class="col-1 master">
				<a id="addToList" class="btn btn-outline-success">Agregar</a>
			</div>
		</div>

		<br>
		<div class="card" >
			<div class="card-header bg-secondary bg-opacity-25">
				<h5 class="card-title">Producto(s)</h5>
			</div>
			<div class="card-body" name="body">
				<div class="table-responsive">
					<table id="detailsTable" class="table">
						<thead>
						<tr>
							<th>CANTIDAD</th>
							<th>DESCRIPCION</th>
							<th>PRECIO UNITARIO</th>
							<th>TOTAL</th>
							<th></th>
						</tr>
						</thead>
						<tbody id="details">
						</tbody>
					</table>
				</div>
			</div>
		</div>

		<br>
		<div class="form-group master" align="right">
			<h5 for="total">TOTAL GS.</h5>
			<div id="total"></div>
		</div>
	</div>
	<br>
	<br>
	<div class="modal-footer">
		<g:link action="index" class="btn-outline-dark btn">Atras</g:link>
		<g:submitButton id = "saveInvoice" name="create" class="btn-outline-success btn" value="${message(code: 'default.button.create.label', default: 'Crear')}" />
	</div>
</div>

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<g:javascript>

	let list = []
	let total = 0
	let productsData = null
	var url="http://localhost:8080/api/"

	fetch(url+'products')
			.then(response => response.json())
			.then(data => {
		productsData = data
	})

	$("#addToList").click(function (e) {
		e.preventDefault();
		let item = {quantity: parseInt($("#quantity").val()), unitPrice: parseInt($("#unitPrice").val()), product: parseInt($("#product").val()), total: parseInt($("#quantity").val()) * parseInt($("#unitPrice").val())}
		if(!isNaN(item.product ) && !isNaN(item.quantity)) {
			list.push(item)
			fillDetails(list)
			// console.log($("[name='condition']").val())
			clearItem();
		}
	});

	function fillDetails(items){
		total = list.reduce((prev,curr)=> prev + curr.total, 0)
        let parent = $("#details");
		parent.html("")
        items.forEach(item => {
			let name = productsData.content.filter(product => product.id == item.product)[0].name
			let barcode = productsData.content.filter(product => product.id == item.product)[0].barcode
			let productItem = '<tr><td>'+item.quantity+'</td><td>' + name + ' ' + barcode + '</td><td>' + item.unitPrice + '</td><td>' + item.total + '</td><td><a id="'+item.product +'" href="#" class="deleteItem">Eliminar</a></td></tr>';
			parent.append(productItem)
        })
		$("#total").html(total)
	}


	//After Add A New Order In The List, Clear Clean The Form For Add More Order.
	function clearItem() {
		$("#product").val('');
		$("#unitPrice").val('');
		$("#quantity").val('');
	}
	// After Add A New Order In The List, If You Want, You Can Remove It.
	$(document).on('click', 'a.deleteItem', function (e) {
		e.preventDefault();
        console.log(e.target.id)
		list = list.filter(item => item.product != e.target.id)
		fillDetails(list)
	});

	function saveInvoice(data) {
		return $.ajax({
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			type: 'POST',
			cache: true,
			url: url + "sale_invoices",
			data: data,
			success: function (result) {
				location = "/saleInvoice/index"
			},
			error: function () {
				alert("Error!")
			}
		});


	}


	$("#saveInvoice").click(function (e) {
		e.preventDefault();

		var data = JSON.stringify({
			stamping: $("#stamping").val(),
			number: $("#number").val(),
			invoiceDate: $("#invoiceDate").val(),
			client: $("#client").val(),
			total: total,
			status: "Creado",
			paymentMethod: $("#paymentMethod").val(),
			condition: $("#contado_check").val(),
			saleInvoiceDetails: list
		});


		$.when(saveInvoice(data)).then(function (response) {
			console.log(response);
		}).fail(function (err) {
			console.log(err);
            console.log(data);
		});
	});

</g:javascript>
