<!DOCTYPE html>
<html>
    <head>
        <meta name="layout" content="main" />
        <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
        <title>Editar :: Healthpotions</title>
    </head>
    <body>
        <div id="edit-product" class="content scaffold-edit" role="main">
            <g:if test="${flash.message}">
            <div class="message" role="status">${flash.message}</div>
            </g:if>
            <g:hasErrors bean="${this.product}">
            <ul class="errors" role="alert">
                <g:eachError bean="${this.product}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
                </g:eachError>
            </ul>
            </g:hasErrors>
            <g:form resource="${this.product}" action="update" method="PUT" >
                <g:hiddenField name="version" value="${this.product?.version}" />
                <g:hiddenField name="id" value="${this.productInstance?.id}" />
                <fieldset class="form">
                        <g:render template="form"/>
                    </fieldset>
                <br>
                <div class="container-fluid col-6" align="right">
                    <fieldset class="buttons">
                        <a href="${createLink(uri: '/product/index')}" class="btn btn-outline-dark"><g:message code="default.link.skip.label" default="Atrás"/></a>
                        <g:submitButton name="edit" class="btn btn-outline-success" value="${message(code: 'default.button.update.label', default: 'Actualizar')}" />
                    </fieldset>
                </div>
            </g:form>
        </div>
    </body>
</html>
