<!DOCTYPE html>
<html lang="en">
<head>
    <meta name="layout" content="main" />
    <g:set var="entityName" value="${message(code: 'product.label', default: 'Product')}" />
    <title>Ver :: Healthpotions</title>
</head>

<body>

<div id="create-type" class="content scaffold-create" role="main">
    <g:hasErrors bean="${this.product}">
        <ul class="errors" role="alert">
            <g:eachError bean="${this.product}" var="error">
                <li <g:if test="${error in org.springframework.validation.FieldError}">data-field-id="${error.field}"</g:if>><g:message error="${error}"/></li>
            </g:eachError>
        </ul>
    </g:hasErrors>

    <div class="container-fluid col-6">
        <div class="row">
            <h4 class="align-content-center" style="padding-top: 5%">Producto</h4>
            <hr>
            <g:if test="${flash.message}">
                <div class="alert alert-success" role="status">
                    El producto ha sido creado!
                </div>
            </g:if>
            <div class="form-group row" style="padding-bottom: 10px; padding-left: 0px">
                <div class="col-1">
                    <g:form resource="${this.product}" method="PUT" action="edit" id="${productInstance?.id}">
                        <g:submitButton action="edit" class="btn btn-sm btn-outline-secondary" name="submit" value="${message(code: 'default.button.edit.label', default: 'Editar')}" />
                    </g:form>
                </div>
                <div class="col-1">
                    <g:form resource="${this.type}" method="DELETE" action="delete" id="${productInstance?.id}">
                        <g:submitButton name="submit" class="btn btn-sm btn-outline-danger" value="${message(code: 'default.button.delete.label', default: 'Eliminar')}" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Are you sure?')}');" />
                    </g:form>
                </div>
            </div>

            <div class="col-md-6 bg-light card-body">
                <div class="fieldcontain ${hasErrors(bean: productInstance, field: 'name', 'error')} required">
                    <div>
                        <h4>${productInstance?.name}</h4>
                    <g:if test="${productInstance.requirePrescription == true}">
                        <h5 class="text-muted">${productInstance?.barcode} <span class="badge bg-success text-light">Receta Medica</span></h5>
                    </g:if>
                    <g:else>
                        <h5 class="text-muted">${productInstance?.barcode} <span class="badge bg-primary text-light">Venta Libre</span></h5>
                    </g:else>
                    </div>
                </div>
            </div>
            <br>
            <div class="form-group row">
                <div class=" col-6">
                    <label for="category" class="col-6 col-form-label"><strong>Categoria:</strong> ${productInstance?.category?.name}</label>
                </div>

                <div class=" col-6">
                    <label for="type" class="col-6 col-form-label"><strong>Tipo:</strong> ${productInstance?.type?.name}</label>
                    <label for="type" class="col-6 col-form-label"></label>
                </div>
            </div>

            <br>

            <br><h6>Precios</h6><hr>
            <div class="form-group row">
                <div class="col-6">
                    <label for="costPrice" class="col-6 col-form-label">Costo</label>
                    <g:field type="number" min="0" readonly="true" name="costPrice" maxlength="100" required="true" class="form-control" value="${productInstance?.costPrice}"/>
                </div>

                <div class="col-6">
                    <label for="price" class="col-6 col-form-label">Venta</label>
                    <g:field type="number"  min="0" readonly="true" name="price" maxlength="100" required="true" class="form-control" value="${productInstance?.price}"/>
                </div>
            </div>
        </div>
    </div>
    <br>
    <div class="container-fluid col-6" align="right">
        <div class="row">
            <hr>
            <fieldset class="buttons">
                <a href="${createLink(uri: '/product/index')}" class="btn btn-outline-dark"><g:message code="default.link.skip.label" default="Atrás"/></a>
            </fieldset>
        </div>
    </div>

</body>
</html>
