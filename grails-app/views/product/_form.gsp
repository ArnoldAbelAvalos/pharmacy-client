<div class="container-fluid col-6">
	<div class="row">
		<h4 class="align-content-center" style="padding-top: 5%">Producto</h4>
		<hr>
		<div class="col-md-6 bg-light card-body">
				<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'name', 'error')} required">
					<label for="name" class="form-label">Nombre<span class="required-indicator">*</span></label>
					<div>
						<g:textField name="name" maxlength="50" required="true" class="form-control" value="${productInstance?.name}"/>
					</div>
				</div>

				<div class="form-group row" style="padding-top: 10px">
					<div class="checkbox">
						<g:checkBox name="requirePrescription" checked="${productInstance?.requirePrescription == true }" value="${productInstance?.requirePrescription}"/> <label>Venta bajo receta</label>
					</div>
				</div><hr>

				<div class="form-group row">
					<div class=" col-6">
						<label for="category" class="col-6 col-form-label">Categoria</label>
						<g:select id="categories" name="category" from="${categories}" optionKey="id" optionValue="name" required="" value="${productInstance?.category?.id}" class="one-to-many form-select"/>
					</div>

					<div class=" col-6">
						<label for="type" class="col-6 col-form-label">Tipo :: Grupo</label>
						<g:select id="types" name="type" from="${types}" optionKey="id" optionValue="name" required="" value="${productInstance?.type?.id}" class="one-to-many form-select"/>
					</div>
				</div>

				<br>

				<div class="fieldcontain ${hasErrors(bean: productInstance, field: 'barcode', 'error')} required">
					<label for="barcode" class="col-12 col-form-label">Código de Barras<span class="required-indicator">*</span></label>
					<div>
						<g:textField name="barcode" maxlength="100" required="true" class="form-control" value="${productInstance?.barcode}"/>
						<small class="text-muted">Compuesto por letras alfabéticas y símbolos permite la indicación de números de producto.</small>
					</div>
				</div>

				<br><h6>Precios</h6><hr>
				<div class="form-group row">
					<div class="col-6">
						<label for="costPrice" class="col-6 col-form-label">Costo</label>
						<g:field type="number" min="0" name="costPrice" maxlength="100" required="true" class="form-control" value="${productInstance?.costPrice}"/>
					</div>

					<div class="col-6">
						<label for="price" class="col-6 col-form-label">Venta</label>
						<g:field type="number"  min="0" name="price" maxlength="100" required="true" class="form-control" value="${productInstance?.price}"/>
					</div>
				</div>

		</div>
	</div>
</div>
