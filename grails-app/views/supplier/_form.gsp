<div class="container-fluid col-6">
	<div class="row">
		<h4 class="align-content-center" style="padding-top: 5%">Proveedor</h4>
		<hr>
		<div class="col-md-6 bg-light card-body">
			<h6>Datos Generales</h6><hr>
			<div class="form-group row">
				<div class="col-12">
					<label for="name" class="col-6 col-form-label">Nombre o Razón Social*</label>
					<g:textField name="name" maxlength="100" required="true" class="form-control" value="${personalData?.name}"/>
				</div>

				<div class="col-6">
					<input type="hidden" name="lastName" value="Proveedor" />
				</div>
			</div>

			<div class="form-group row">
				<input type="hidden" name="document" value="123456" />

				<div class="col-6">
					<label for="ruc" class="col-6 col-form-label">RUC*</label>
					<g:textField name="ruc" maxlength="100" required="true" class="form-control" value="${personalData?.ruc}"/>
				</div>

				<div class="col-6">
					<label for="address" class="col-6 col-form-label">Dirección*</label>
					<g:textField name="address" maxlength="100" required="true" class="form-control" value="${personalData?.address}"/>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-6">
					<input type="hidden" name="dob" value="2021-10-10" />
				</div>
			</div>

			<br><h6>Datos de Contacto</h6><hr>
			<div class="form-group row">
				<div class="col-6">
					<label for="phone" class="col-6 col-form-label">Teléfono*</label>
					<g:textField name="phone" maxlength="100" required="true" class="form-control" value="${personalData?.phone}"/>
				</div>

				<div class="col-6">
					<label for="email" class="col-6 col-form-label">Email*</label>
					<g:textField name="email" maxlength="100" required="true" class="form-control" value="${personalData?.email}"/>
				</div>
			</div>
		</div>
	</div>
</div>
