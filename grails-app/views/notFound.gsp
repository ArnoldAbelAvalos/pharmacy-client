<!DOCTYPE html>
<html>
<head>
    <link crossorigin="anonymous" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" rel="stylesheet"/>
    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
    <title><g:if env="development">Oops! :: Healthpotions</g:if><g:else>Error :: Healthpotions</g:else></title>
</head>
<body>
<div class="container" style="padding-top: 50px">
    <div class="jumbotron">
        <h1 class="display-5"><i class="fa fa-search"></i> ERROR - 404</h1>
        <hr class="my-4">
        <p class="lead"> La pagina que has solicitado no ha sido encontrada.</p>
        <a href="/" class="btn btn-primary btn-round">Pagina Principal</a>
    </div>

</div>
</body>
</html>


