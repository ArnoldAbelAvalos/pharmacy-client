<html>
<head>
    <meta name="layout" content="main"/>
    <g:set var="entityName" value="${message(code: 'contact.label', default: 'Contacto')}"/>
    <title>Lista :: Healthpotions</title>
    <asset:stylesheet src="index.css"/>
</head>
<body>
<div class="container centered">
    <br><h1 class="title">Mensajes de Contacto</h1><br>

    <g:if test="${flash.message}">
        <div class="alert alert-success" role="status">${flash.message}</div>
    </g:if>

    <table class="table" id="table">
        <thead>
        <tr>
            <th scope="col">Nombre Completo</th>
            <th scope="col">Razón</th>
            <th scope="col">Mensaje</th>
            <th scope="col">Email</th>
            <th scope="col">Teléfono</th>
            <th scope="col"></th>
        </tr>
        </thead>
        <tbody>
        <g:each in="${contacts}" status="i" var="contactInstance">
            <tr>
                <td>${contactInstance?.fullname}</td>
                <g:if test="${contactInstance.reason == "Consulta"}">
                    <td><span class="badge bg-success text-light">${contactInstance.reason}</span></td>
                </g:if>
                <g:elseif test="${contactInstance.reason == "Reclamo"}">
                    <td><span class="badge bg-danger text-light">${contactInstance.reason}</span></td>
                </g:elseif>
                <g:elseif test="${contactInstance.reason == "Sugerencia"}">
                    <td><span class="badge bg-warning text-light">${contactInstance.reason}</span></td>
                </g:elseif>
                <g:else>
                    <td><span class="badge bg-dark text-light">${contactInstance.reason}</span></td>
                </g:else>
                <td>${contactInstance?.message}</td>
                <td>${contactInstance?.email}</td>
                <td>${contactInstance?.phone}</td>
                <td><g:form resource="${this.contactInstance}" method="DELETE" action="delete" id="${contactInstance?.id}"
                            class="justify-content-between">
                    <button class="btn btn-sm btn-danger" type="submit" onclick="return confirm('${message(code: 'default.button.delete.confirm.message', default: 'Estas seguro?')}');">
                        <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-x-circle" viewBox="0 0 16 16">
                            <path d="M8 15A7 7 0 1 1 8 1a7 7 0 0 1 0 14zm0 1A8 8 0 1 0 8 0a8 8 0 0 0 0 16z"></path>
                            <path d="M4.646 4.646a.5.5 0 0 1 .708 0L8 7.293l2.646-2.647a.5.5 0 0 1 .708.708L8.707 8l2.647 2.646a.5.5 0 0 1-.708.708L8 8.707l-2.646 2.647a.5.5 0 0 1-.708-.708L7.293 8 4.646 5.354a.5.5 0 0 1 0-.708z"></path>
                        </svg>
                    </button>
                </g:form>
                </td>
            </tr>

        </g:each>
        </tbody>
    </table>

    <nav aria-label="...">
        <ul class="pagination" id="pagination">
            <g:each in="${pagination}" status="i" var="pageNum">
                <g:if test="${pageNum == page}">
                    <li class="page-item active"><g:link class="page-link" action="index"
                                                         id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
                <g:if test="${pageNum != page}">
                    <li class="page-item"><g:link class="page-link" action="index"
                                                  id="${pageNum}">${pageNum}</g:link></li>
                </g:if>
            </g:each>

        </ul>
    </nav>
</div>
</body>
</html>