<div class="container-fluid col-6">
	<div class="row">
		<div class="col-md-6 bg-light card-body">
			<div class="form-group row">
				<div>
					<label for="fullname" class="col-6 col-form-label">Nombre Completo*</label>
					<g:textField name="fullname" maxlength="100" required="true" class="form-control"/>
				</div>
			</div>

			<div class="form-group row">
				<div class="col-6">
					<label for="phone" class="col-6 col-form-label">Teléfono*</label>
					<g:textField name="phone" maxlength="100" required="true" class="form-control"/>
				</div>

				<div class="col-6">
					<label for="email" class="col-6 col-form-label">Email*</label>
					<g:textField name="email" maxlength="100" required="true" class="form-control"/>
				</div>
			</div>
			<br><hr>
			<div class="form-group row">
				<div>
					<label class="col-6 col-form-label">Motivo</label>
					<select class="form-select" onchange="copyTextValue()" id="select">
						<option selected>Selecciona</option>
						<option value="Reclamo">Reclamo</option>
						<option value="Consulta">Consulta sobre un producto</option>
						<option value="Sugerencia">Sugerencia</option>
						<option value="Otro">Otro</option>
					</select>
				</div>
			</div>

			<td><input type="hidden" id="destination"> </td>
			<br>
			<div class="form-group">
				<label>Mensaje</label>
				<textarea class="form-control" id="message" name="message" rows="4"></textarea>
			</div>
		</div>
	</div>
	<br>
	<div class="container-fluid col-6"  align="center">
		<fieldset class="buttons">
			<g:submitButton id="create" name="create" class="btn-outline-success btn"
							value="Enviar"/>
		</fieldset>
	</div>
</div>

<script src="https://ajax.aspnetcdn.com/ajax/jQuery/jquery-3.3.1.min.js"></script>
<g:javascript>

	function copyTextValue() {
		var e = document.getElementById("select");
		var val = e.options[e.selectedIndex].value;
		document.getElementById("destination").value = val;
	}

	function save(data) {
		return $.ajax({
			contentType: 'application/json; charset=utf-8',
			dataType: 'json',
			type: 'POST',
			cache: true,
			url: "http://localhost:8080/api/contacts",
			data: data,
			success: function (result) {
				location = "/contact/index"
			},
			error: function () {
				alert("Error!")
			}
		});
	}


	$("#create").click(function (e) {
		e.preventDefault();

		var data = JSON.stringify({
			fullname: $("#fullname").val(),
			phone: $("#phone").val(),
			email: $("#email").val(),
			reason: $("#destination").val(),
			message: $("#message").val(),
		});

		$.when(save(data)).then(function (response) {
			console.log(response);
		}).fail(function (err) {
			console.log(err);
			console.log(data);
		});
    });

</g:javascript>