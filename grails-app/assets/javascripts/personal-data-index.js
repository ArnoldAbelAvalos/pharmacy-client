

let spinner = document.getElementById('spinner')
let buttons = document.getElementById('buttons')
let table = document.getElementById('table')
let tableBody = document.getElementById('table-body')
let pagination = document.getElementById('pagination')
let currentPage = 0;
const Base_URL = 'http://localhost:8080/api/'
const path = 'personal_data'

function getNotDeleted(path, page, callback){
    fetch(Base_URL + path +'/?page='+ page)
        .then(response => response.json())
        .then(data => callback(data))
        .catch(e => {
            getNotDeleted()
        })
}

function renderView(data){
        spinner.classList.add('hidden')
        table.classList.remove('hidden')
        buttons.classList.remove('hidden')
        pagination.classList.remove('hidden')
        tableBody.innerHTML = ""

        renderTable(data)
        renderPagination(data)
}

function renderTable(data){
    data.content.forEach(item =>{
        let displayable = {...item}
        delete displayable.id
        delete displayable.createdAt
        delete displayable.updatedAt
        delete displayable.deletedAt

        let row = document.createElement("tr");
        for (const [key, value] of Object.entries(displayable)) {
            let field = document.createElement("td");
            field.innerHTML = value
            row.appendChild(field)
        }
        let buttons = document.createElement("td");

        buttons.classList.add('space-around')

        let viewButton = document.createElement("a");
        let editButton = document.createElement("a");
        let deleteButton = document.createElement("button");

        viewButton.innerText = "👁"
        editButton.innerText = "🔧"
        deleteButton.innerText = "❌"

        viewButton.href = "/personalData/show/" + item.id
        editButton.href = "/personalData/edit/" + item.id
        deleteButton.addEventListener('click', e=>{
            fetch('/personalData/delete/' + item.id , {
                method: "DELETE"
            }).then(response => {
                location.reload()
            })
        })


        viewButton.classList.add(...['btn', 'btn-primary'])
        editButton.classList.add(...['btn', 'btn-warning'])
        deleteButton.classList.add(...['btn', 'btn-danger'])

        buttons.appendChild(viewButton)
        buttons.appendChild(editButton)
        buttons.appendChild(deleteButton)
        row.appendChild(buttons)

        tableBody.appendChild(row)
    })
}

function renderPagination(data){
    pagination.innerHTML = ''
    let amp = 3
    let lowerBound = currentPage - amp < 0 ? 0 : currentPage - amp
    let upperBound = currentPage + amp > data.totalPages ? data.totalPages : currentPage + amp

    for (let i = lowerBound; i <upperBound; i++){
        let li = document.createElement("li");
        li.classList.add('page-item')
        if (i == data.number){
            li.classList.add('active')
        }
        li.addEventListener('click', x=> {
            getNotDeleted(path, i, renderView)
            currentPage = i
        })

        let a = document.createElement("a");
        a.classList.add('page-link')
        a.innerText = i + 1;

        li.appendChild(a)

        pagination.appendChild(li)
    }
}


getNotDeleted(path, currentPage, renderView)