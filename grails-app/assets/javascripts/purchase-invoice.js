let addDetailBtn = document.getElementById("addDetailBtn")
let submitBtn = document.getElementById('submitBtn')

let details = []
let total = 0
let url = "http://localhost:8080/api/"


let productsLoading = true
let productsData = null
let productError = null

let supplierLoading = true
let supplierData = null
let supplierError = null

let personalDataLoading = false
let personalDataData = null
let personalDataError = null

fetch(url + 'products')
    .then(response => response.json())
    .then(data => {
        productsData = data
        productsLoading = false
    })
    .catch((error) => {
        productError = error
        productsLoading = false
    })

fetch(url + 'suppliers')
    .then(response => response.json())
    .then(data => {
        supplierData = data
        supplierLoading = false
    })
    .catch((error) => {
        supplierError = error
        supplierLoading = false
    })
isLoading()

function isLoading() {
    console.log("isLoading?")
    let finishLoading = !productsLoading && !supplierLoading
    let errors = productError != null && supplierError != null
    if (!finishLoading) {
        document.getElementById("spinner").hidden = false
        document.getElementById("form").hidden = true
        setTimeout(isLoading, 1000)
    }
    if (finishLoading && !errors) {
        console.log("finished")
        productsData.content.forEach(product => {
            document.getElementById("products").innerHTML += `<option value="${product.id}" selected>${product.name}</option>`
        })

        supplierData.content.map( async supplier=> {
            let personalData
            await fetch(url + "personal_data/" + supplier.personalData)
                .then(result => result.json())
                .then(data => personalData = data )
                .catch(e => console.log("Error: ", e))
            document.getElementById("suppliers").innerHTML += `<option value="${supplier.id}" selected>${personalData.name}</option>`
            return {id: supplier.id, personalData: personalData }
        })



        document.getElementById("spinner").hidden = true
        document.getElementById("form").hidden = false

    }
    if (finishLoading && errors) {
        console.log("Error Ocurred")
    }
}

submitBtn.addEventListener('click', e => {
    let purchaseInvoice = {
        supplier: +document.getElementById("suppliers").value,
        condition: document.querySelector('input[name="condition"]:checked').value,
        total: total,
        status: "Creado",
        paymentMethod: document.getElementById("paymentMethod").value,
        stamped: document.getElementById("stamped").value,
        number: +document.getElementById("number").value,
        details: details
    }

    fetch(url + 'purchase_invoice/details', {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
        body: JSON.stringify(purchaseInvoice)
    })
        .then(response => response.json())
        .then(data => {
            console.log('Success: ', data)
            document.location = "/purchaseInvoice"
        })
        .catch((error) => {
            console.error('Error:', error);
        });

})

addDetailBtn.addEventListener('click', () => {

    let price = productsData.content.filter(product => product.id == +document.getElementById("products").value)[0].price
    let item = {
        quantity: +document.getElementById("quantity").value,
        product: +document.getElementById("products").value,
        unitPrice: price,
        purchaseInvoice: 0
    }
    item.total = item.quantity * item.unitPrice
    details.push(item)
    drawDetails()
    clearInputs()
})


function drawDetails() {
    let detailsContainer = document.getElementById("details")
    detailsContainer.innerHTML = ""
    total = details.reduce((prev, curr) => prev + curr.total, 0)
    details.forEach(item => {
        let name = productsData.content.filter(product => product.id == item.product)[0].name
        let productItem = '<tr><td>' + item.quantity + '</td><td>' + name + '</td><td>' + item.unitPrice + '</td><td>' + item.total + '</td><td><button id="' + item.product + '" href="#" class="btn-danger">❌</button></td></tr>';
        detailsContainer.innerHTML += productItem
        document.getElementById(item.product).addEventListener('click', e => {
            console.log(e.target.id)
            details = details.filter(item => item.product != e.target.id)
            drawDetails()
        })
    })
    document.getElementById('total').innerHTML = total
}

function clearInputs() {
    document.getElementById("quantity").value = "1"
    document.getElementById("product")
}


