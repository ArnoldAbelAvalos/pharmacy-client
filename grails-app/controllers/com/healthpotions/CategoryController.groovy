package com.healthpotions

import com.healthpotions.beans.category.CategoryB
import com.healthpotions.beans.role.RoleBean
import com.healthpotions.services.category.CategoryServiceImpl
import com.healthpotions.services.category.ICategoryService
import com.healthpotions.utils.PaginationUtils

import static org.springframework.http.HttpStatus.*

class CategoryController {
    def CategoryServiceImpl categoryService


    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]


    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def query = params.get("name") == null ? "" : params.get("name")
        //def pair = userService.getAllWithQuery(page, query)
        def pair = categoryService.getAllWithQuery(page,query)
        def categories = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([categories:categories, page:page, pagination:pagination])
    }

    def show(Long id) {
        def categoryInstance = categoryService.getById(id);
        respond([categoryInstance: categoryInstance])
    }

    def create() {
        [categoryInstance: new CategoryB(params.id, params.name)]
    }

    def save() {
        def newCategory = new CategoryB(null, params.name)
        def categoryInstance = categoryService.save(newCategory)
        if (!categoryInstance.getId()) {
            render(view: "create", model: [categoryInstance: categoryInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'category.label', default: 'Categoria'),
                categoryInstance.get_name()
        ])
        redirect(action: "show", id: categoryInstance.getId())
    }

    def edit(Long id) {
        def categoryInstance = categoryService.getById(id)
        if (!categoryInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'category.label', default: 'Categoria'),
                    id
            ])
            redirect(action: "index")
            return
        }

        [categoryInstance: categoryInstance]
    }

    def update(CategoryB bean) {
        categoryService.update(bean, bean.getId())

        flash.message = message(code: 'default.updated.message', args: [
                message(code: 'category.label', default: 'Categoria'),
                categoryService.getById(bean.getId()).get_name()
        ])
        redirect(action: "index")
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }
        categoryService.delete(id)

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'category.label', default: 'Categoria'),
                id
        ])

        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'category.label', default: 'Category'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
