package com.healthpotions

import com.healthpotions.beans.role.RoleBean
import com.healthpotions.beans.type.TypeB
import com.healthpotions.services.type.ITypeService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException
import org.springframework.beans.factory.annotation.Autowired

import static org.springframework.http.HttpStatus.*
import com.distribuidos.healthpotions.dto.type.TypeDTO;

class TypeController {

    ITypeService typeService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def pair = typeService.getAll(page)
        def types = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([types:types, page:page, pagination:pagination])
    }

    def show(Long id) {
        def typeInstance = typeService.getById(id);
        respond([typeInstance: typeInstance])
    }

    def create() {
        [typeInstance: new TypeB(params.id, params.name)]
    }

    def save() {
        def newType = new TypeB(null, params.name)
        def typeInstance = typeService.save(newType)
        if (!typeInstance.getId()) {
            render(view: "create", model: [typeInstance: typeInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'type.label', default: 'Type'),
                typeInstance.getId()
        ])
        redirect(action: "show", id: typeInstance.getId())
    }

    def edit(Long id) {
        def typeInstance = typeService.getById(id)
        if (!typeInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'type.label', default: 'Type'),
                    id
            ])
            redirect(action: "index")
            return
        }

        [typeInstance: typeInstance]
    }

    def update(TypeB bean) {
        typeService.update(bean, bean.getId())
        flash.message = message(code: 'default.updated.message', args: [
                message(code: 'type.label', default: 'Tipo'),
                typeService.getById(bean.getId()).getName()
        ])
        redirect(action: "index")
    }

    def delete(Long id){
        typeService.delete(id);
        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'type.label', default: 'Tipo'),
                typeService.getById(id).getName()
        ])
        redirect(action: "index")
    }

    protected void notFound() {

    }
}
