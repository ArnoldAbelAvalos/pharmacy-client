package com.healthpotions

import com.healthpotions.beans.personal_data.PersonalDataBean
import com.healthpotions.beans.supplier.SupplierB
import com.healthpotions.beans.user.UserBean
import com.healthpotions.services.personal_data.IPersonalDataService
import com.healthpotions.services.supplier.ISupplierService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class SupplierController {

    ISupplierService supplierService
    IPersonalDataService personalDataService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def pair = supplierService.getAll(page)
        def suppliers = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([suppliers:suppliers, page:page, pagination:pagination])
    }

    def show(Long id) {
        def supplierInstance = supplierService.getById(id);
        respond([supplierInstance: supplierInstance])
    }

    def create() {
        respond([])
    }

    def save(SupplierB supplierInstance, PersonalDataBean personalData) {
        supplierInstance.setPersonalData(personalData)
        if ( supplierInstance == null) {
            notFound()
            return
        }
        try {
            supplierService.save(supplierInstance)
        } catch (ValidationException e) {
            respond supplierInstance.errors, view:'create'
            return errors.getFieldError()
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'supplier.label', default: 'Proveedor'),
                supplierInstance.getPersonalData().getName()
        ])
        redirect(action: "index")
    }

    def edit(Long id) {
        SupplierB supplierInstance = supplierService.getById(id)
        respond([supplierInstance:supplierInstance, personalData:supplierInstance.getPersonalData()])
    }

    def update(SupplierB supplierInstance, PersonalDataBean personalData) {
        supplierInstance.setPersonalData(personalData)

        try {
            supplierInstance = supplierService.update(supplierInstance)
        } catch (ValidationException e) {
            respond supplierInstance.errors, view:'edit'
            return
        }

        flash.message = message(code: 'default.updated.message', args: [
                message(code: 'supplier.label', default: 'Proveedor'),
                supplierInstance.getPersonalData().getName()
        ])
        redirect(action: "index")
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        supplierService.delete(id)

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'supplier.label', default: 'Proveedor'),
                ''
        ])
        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'supplier.label', default: 'Proveedor'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
