package com.healthpotions

import com.healthpotions.beans.order.OrderB
import com.healthpotions.services.order.IOrderService
import com.healthpotions.services.product.IProductService
import com.healthpotions.services.supplier.ISupplierService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException
import java.time.LocalDate
import static org.springframework.http.HttpStatus.*

class OrderController {

    IOrderService orderService

    IProductService productService

    ISupplierService supplierService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def pair = orderService.getAll(page)
        def orders = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([orders:orders, page:page, pagination:pagination])
    }

    def show(Long id) {
        def order = orderService.getById(id)
        respond([order: order])
    }

    def create() {
        respond([order: new OrderB(), products: productService.getAll(0).getaValue(), date: LocalDate.now().toString(), suppliers: supplierService.getAll(0).getaValue()])
    }

    def save(OrderB order) {
        order = orderService.save(order)

        if (!order?.getId()) {
            render(view: "create", model: [order: order])
            return
        }
        redirect(action: "list")
    }

    def edit(Long id) {
        respond orderService.get(id)
    }

    def update(OrderB order) {
        if (order == null) {
            notFound()
            return
        }

        try {
            orderService.save(order)
        } catch (ValidationException e) {
            respond order.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'order.label', default: 'Order'), order.id])
                redirect order
            }
            '*'{ respond order, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        orderService.delete(id)

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'order.label', default: 'Orden'),
                id
        ])

        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'order.label', default: 'Order'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
