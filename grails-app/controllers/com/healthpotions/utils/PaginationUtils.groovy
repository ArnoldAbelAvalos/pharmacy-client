package com.healthpotions.utils

class PaginationUtils {
    static Range<Long> getPaginationRange(Long page, Long totalPages){
        if (totalPages < 0 ) return new EmptyRange<Long>()
        def amp = 3
        def lower = page - amp >= 0? page - amp : 0
        def upper = page + amp >= totalPages ? totalPages : page + amp
        return lower..upper
    }
}
