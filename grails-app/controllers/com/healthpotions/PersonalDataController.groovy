package com.healthpotions

import com.healthpotions.beans.personal_data.PersonalDataBean
import com.healthpotions.services.personal_data.IPersonalDataService
import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.*

class PersonalDataController {

    IPersonalDataService personalDataService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index() {
        //def list = personalDataService.getAll(0L)
        respond([])
    }

    def show(Long id) {
        PersonalDataBean bean = personalDataService.getById(id)
        respond([personalDataBean:bean])
    }

    def create() {
        respond([personalData:new PersonalDataBean()])
    }

    def save(PersonalDataBean personalData) {
        if (personalData == null) {
            notFound()
            return
        }

        try {
           personalData = personalDataService.save(personalData)
        } catch (ValidationException e) {
            respond personalData.errors, view:'create', model: [personalData:personalData]
            return
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'personalData.label', default: 'Datos Personales'),
                personalData.getName()
        ])
        redirect(action: "show", id: personalData.getId())

    }

    def edit(Long id) {
        respond([personalData:personalDataService.getById(id)])
    }

    def update(PersonalDataBean personalData) {
        if (personalData == null) {
            notFound()
            return
        }

        try {
            personalData = personalDataService.update(personalData);
        } catch (ValidationException e) {
            respond personalData.errors, view:'edit'
            return
        }

        flash.message = message(code: 'default.updated.message', args: [
                message(code: 'personalData.label', default: 'Dato Personal'),
                personalData.getName()
        ])
        redirect(action: "show", id: personalData.getId())
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        personalDataService.delete(id)

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'personalData.label', default: 'Datos Personal'),
                ''
        ])
        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'personalData.label', default: 'PersonalData'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
