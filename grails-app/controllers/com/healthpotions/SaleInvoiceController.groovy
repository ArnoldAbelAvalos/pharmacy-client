package com.healthpotions

import com.healthpotions.beans.saleInvoice.SaleInvoiceB
import com.healthpotions.resources.personal_data.IPersonalDataResource
import com.healthpotions.services.client.IClientService
import com.healthpotions.services.personal_data.IPersonalDataService
import com.healthpotions.services.product.IProductService
import com.healthpotions.services.saleInvoice.ISaleInvoiceDetailService
import com.healthpotions.services.saleInvoice.SaleInvoiceService

import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import com.healthpotions.services.saleInvoice.ISaleInvoiceService
import grails.validation.ValidationException
import com.healthpotions.utils.PaginationUtils

import static org.springframework.http.HttpStatus.*

class SaleInvoiceController {

    SaleInvoiceService saleInvoiceService
    ISaleInvoiceDetailService saleInvoiceDetailService
    IProductService productService
    IClientService clientService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def query = params.get("number") == null ? "" : params.get("number")

        def pair = saleInvoiceService.getAllWithQuery(page, query)
        def saleInvoices = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([saleInvoices:saleInvoices, page:page, pagination:pagination])
    }

    def show(Long id) {
        def saleInstance = saleInvoiceService.getById(id);
        respond([saleInstance: saleInstance])
    }

    def create() {
        respond([saleInstance: new SaleInvoiceB(), products: productService.getAll(0).getaValue(), clients: clientService.getAll(0).getaValue(), date: LocalDate.now().toString()])
    }

    def save(SaleInvoiceB saleInstance) {

        saleInstance = saleInvoiceService.save(saleInstance)

        if (!saleInstance?.getId()) {
            render(view: "create", model: [saleInstance: saleInstance])
            return
        }
        redirect(action: "list")
    }


    def edit(Long id) {
        respond saleInvoiceService.getById(id)
    }

    def update(SaleInvoiceB saleInvoice) {
        if (saleInvoice == null) {
            notFound()
            return
        }

        try {
            saleInvoiceService.save(saleInvoice)
        } catch (ValidationException e) {
            respond saleInvoice.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'saleInvoice.label', default: 'SaleInvoice'), saleInvoice.id])
                redirect saleInvoice
            }
            '*'{ respond saleInvoice, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        saleInvoiceService.delete(id)

        flash.message = message(code: 'default.cancel.message', args: [
                message(code: 'saleInvoice.label', default: 'Factura de Venta Nro.'),
                id
        ])

        redirect(action: "index")

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'saleInvoice.label', default: 'SaleInvoice'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
