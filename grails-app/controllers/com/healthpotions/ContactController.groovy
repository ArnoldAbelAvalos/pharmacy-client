package com.healthpotions


import com.healthpotions.beans.contact.ContactB
import com.healthpotions.services.contact.IContactService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ContactController {

    IContactService contactService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def pair = contactService.getAll(page)
        def contacts = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([contacts:contacts, page:page, pagination:pagination])
    }

    def show(Long id) {
        ContactB bean = contactService.getById(id)
        respond([contactInstance:bean])
    }

    def create() {
        respond([contact:new ContactB()])
    }

    def save(ContactB contact) {
        if (contact == null) {
            notFound()
            return
        }

        try {
            contact = contactService.save(contact)
        } catch (ValidationException e) {
            respond contact.errors, view:'create', model: [contact:contact]
            return
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'contact.label', default: 'Mensaje de Contacto'),
        ])
        redirect(action: "show", id: contact.getId())
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

       contactService.delete(id)

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'contact.label', default: 'Mensaje de Contacto'),
                ''
        ])
        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'contact.label', default: 'Mensaje de Contacto'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }

}
