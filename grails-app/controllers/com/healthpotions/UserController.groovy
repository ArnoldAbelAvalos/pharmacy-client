package com.healthpotions

import com.healthpotions.beans.personal_data.PersonalDataBean
import com.healthpotions.beans.role.RoleBean
import com.healthpotions.beans.user.UserBean
import com.healthpotions.services.personal_data.IPersonalDataService
import com.healthpotions.services.role.IRoleService
import com.healthpotions.services.user.UserService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException


import static org.springframework.http.HttpStatus.*

class UserController {

    UserService userService
    IRoleService roleService
    IPersonalDataService personalDataService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def query = params.get("name") == null ? "" : params.get("name")
        def pair = userService.getAllWithQuery(page, URLEncoder.encode(query, "UTF-8"))
        def users = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([users:users, page:page, pagination:pagination, query:new RoleBean(0,query,"")])
    }

    def create() {
        def roles = roleService.getAll(0L)
        respond([roles:roles.getaValue()])
    }

    def save(UserBean user, PersonalDataBean personalData) {
        def roles = request.getParameterValues("roles")
        if (user == null || personalData == null || roles == null) {
            notFound()
            return
        }
        user.setPersonalData(personalData)
        user.setRoles(roles.toList().stream().map(idStr -> {
            Long id = Long.valueOf(idStr)
            RoleBean role = new RoleBean()
            role.setId(id)
            return role
        }).toList())


        try {
           user = userService.save(user)
        } catch (ValidationException e) {
            respond user.errors, view:'create'
            return
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'user.label', default: 'Usuario'),
                user.getUserName()
        ])
        redirect(action: "index")
    }

    def edit(Long id) {
        UserBean userBean = userService.getById(id)
        def roles = roleService.getAll(0L)
        respond([userBean:userBean, personalData:userBean.getPersonalData(), roles:roles.getaValue()])
    }

    def update(UserBean user, PersonalDataBean personalData) {
        def roles = request.getParameterValues("roles")
        if (user == null || personalData == null || roles == null) {
            notFound()
            return
        }
        user.setPersonalData(personalData)
        user.setRoles(roles.toList().stream().map(idStr -> {
            Long id = Long.valueOf(idStr)
            RoleBean role = new RoleBean()
            role.setId(id)
            return role
        }).toList())

        try {
            user = userService.update(user)
        } catch (ValidationException e) {
            respond user.errors, view:'edit'
            return
        }

        flash.message = message(code: 'default.updated.message', args: [
                message(code: 'user.label', default: 'Usuario'),
                user.getUserName()
        ])
        redirect(action: "index")
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        userService.delete(id)

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'user.label', default: 'Usuario'),
                ''
        ])
        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'user.label', default: 'User'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
