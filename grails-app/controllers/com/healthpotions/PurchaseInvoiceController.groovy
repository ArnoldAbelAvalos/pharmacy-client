package com.healthpotions

import com.healthpotions.beans.purchase_invoice.PurchaseInvoiceBean
import com.healthpotions.services.purchase_invoice.IPurchaseInvoiceService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException

import java.time.LocalDate
import java.time.format.DateTimeFormatter

import static org.springframework.http.HttpStatus.*

class PurchaseInvoiceController {

    IPurchaseInvoiceService purchaseInvoiceService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def pair = purchaseInvoiceService.getAll(page)
        def purchaseInvoices = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([purchaseInvoices:purchaseInvoices, page:page, pagination:pagination])
    }

    def show(Long id) {
        def purchaseInvoice = purchaseInvoiceService.getById(id)
        respond([purchaseInvoice: purchaseInvoice])
    }

    def create() {
        respond([stamped:"000-000-1", date: LocalDate.now().toString()])
    }

    def save(PurchaseInvoiceBean purchaseInvoice) {
        if (purchaseInvoice == null) {
            notFound()
            return
        }
    }

    def edit(Long id) {
        respond purchaseInvoiceService.get(id)
    }

    def update(PurchaseInvoiceBean purchaseInvoice) {
        if (purchaseInvoice == null) {
            notFound()
            return
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        purchaseInvoiceService.delete(id)
        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'purchaseInvoice.label', default: 'Factura de Compra'),
                id
        ])

        redirect(action: "index")

    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'purchaseInvoice.label', default: 'PurchaseInvoice'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
