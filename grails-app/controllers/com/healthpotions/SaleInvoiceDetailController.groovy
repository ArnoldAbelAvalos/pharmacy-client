package com.healthpotions

import com.healthpotions.beans.saleInvoice.SaleInvoiceDetailB
import com.healthpotions.services.saleInvoice.SaleInvoiceDetailService
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class SaleInvoiceDetailController {

    SaleInvoiceDetailService saleInvoiceDetailService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Integer max) {
        params.max = Math.min(max ?: 10, 100)
        respond saleInvoiceDetailService.list(params), model:[saleInvoiceDetailCount: saleInvoiceDetailService.count()]
    }

    def show(Long id) {
        respond saleInvoiceDetailService.get(id)
    }

    def create() {
    }

    def save(SaleInvoiceDetailB saleInvoiceDetail) {
        if (saleInvoiceDetail == null) {
            notFound()
            return
        }

        try {
            saleInvoiceDetailService.save(saleInvoiceDetail)
        } catch (ValidationException e) {
            respond saleInvoiceDetail.errors, view:'create'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.created.message', args: [message(code: 'saleInvoiceDetail.label', default: 'SaleInvoiceDetail'), saleInvoiceDetail.id])
                redirect saleInvoiceDetail
            }
            '*' { respond saleInvoiceDetail, [status: CREATED] }
        }
    }

    def edit(Long id) {
        respond saleInvoiceDetailService.get(id)
    }

    def update(SaleInvoiceDetailB saleInvoiceDetail) {
        if (saleInvoiceDetail == null) {
            notFound()
            return
        }

        try {
            saleInvoiceDetailService.save(saleInvoiceDetail)
        } catch (ValidationException e) {
            respond saleInvoiceDetail.errors, view:'edit'
            return
        }

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.updated.message', args: [message(code: 'saleInvoiceDetail.label', default: 'SaleInvoiceDetail'), saleInvoiceDetail.id])
                redirect saleInvoiceDetail
            }
            '*'{ respond saleInvoiceDetail, [status: OK] }
        }
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        saleInvoiceDetailService.delete(id)

        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.deleted.message', args: [message(code: 'saleInvoiceDetail.label', default: 'SaleInvoiceDetail'), id])
                redirect action:"index", method:"GET"
            }
            '*'{ render status: NO_CONTENT }
        }
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'saleInvoiceDetail.label', default: 'SaleInvoiceDetail'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
