package com.healthpotions

import com.healthpotions.beans.category.CategoryB
import com.healthpotions.beans.client.ClientB
import com.healthpotions.beans.product.ProductBean
import com.healthpotions.beans.type.TypeB
import com.healthpotions.services.category.ICategoryService
import com.healthpotions.services.product.ProductService
import com.healthpotions.services.type.ITypeService
import com.healthpotions.services.product.IProductService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException

import static org.springframework.http.HttpStatus.*

class ProductController {

    ProductService productService
    ITypeService typeService
    ICategoryService categoryService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def query = params.get("name") == null ? "" : params.get("name")
        def pair = productService.getAllWithQuery(page, URLEncoder.encode(query, "UTF-8"))
        def products = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([products:products, page:page, pagination:pagination])

    }

    def show(Long id) {
        def productInstance = productService.getById(id);
        respond([productInstance: productInstance])
    }

    def create() {
        respond([productInstance: new ProductBean(), types: typeService.getAll(0).getaValue(), categories: categoryService.getAll(0).getaValue() ])
    }

    def save(ProductBean productInstance) {
        productInstance.setType(typeService.getById(Long.valueOf(params.type)))
        productInstance.setCategory(categoryService.getById(Long.valueOf(params.category)))
        productInstance = productService.save(productInstance)
        if (!productInstance.getId()) {
            render(view: "create", model: [productInstance: productInstance])
            return
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'product.label', default: 'Producto'),
                productInstance.getName()
        ])
        redirect(action: "show", id: productInstance.getId())
    }

    def edit(Long id) {
        def productInstance = productService.getById(id)
        if (!productInstance) {
            flash.message = message(code: 'default.not.found.message', args: [
                    message(code: 'product.label', default: 'Producto'),
                    id
            ])
            redirect(action: "index")
            return
        }

        [productInstance: productInstance, types: typeService.getAll(0).getaValue(), categories: categoryService.getAll(0).getaValue() ]
    }

    def update(ProductBean bean, CategoryB cat, TypeB type) {
        bean.setCategory(cat)
        bean.setType(type)
        productService.update(bean, bean.getId())

        flash.message = message(code: 'default.updated.message', args: [
                message(code: 'product.label', default: 'Producto'),
                productService.getById(bean.getId()).getName()
        ])
        redirect(action: "index")

    }

    def delete(Long id) {
        productService.delete(id);

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'product.label', default: 'Producto'),
                id
        ])
        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'product.label', default: 'Product'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
