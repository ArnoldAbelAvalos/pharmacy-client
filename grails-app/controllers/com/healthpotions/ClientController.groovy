package com.healthpotions

import com.healthpotions.beans.client.ClientB
import com.healthpotions.beans.personal_data.PersonalDataBean
import com.healthpotions.services.client.IClientService
import com.healthpotions.services.personal_data.IPersonalDataService
import com.healthpotions.utils.PaginationUtils
import grails.validation.ValidationException
import static org.springframework.http.HttpStatus.*

class ClientController {

    IClientService clientService
    IPersonalDataService personalDataService

    static allowedMethods = [save: "POST", update: "PUT", delete: "DELETE"]

    def index(Long page) {
        if(page == null){
            page = 0L
        }
        def pair = clientService.getAll(page)
        def clients = pair.getaValue()
        def totalPages = pair.getbValue()
        def pagination = PaginationUtils.getPaginationRange(page,totalPages - 1)
        respond([clients:clients, page:page, pagination:pagination])
    }

    def show(Long id) {
        def clientInstance = clientService.getById(id);
        respond([clientInstance: clientInstance])
    }

    def create() {
        respond([])
    }

    def save(ClientB clientInstance, PersonalDataBean personalData) {
        clientInstance.setPersonalData(personalData)
        if ( clientInstance == null) {
            notFound()
            return
        }

        try {
            clientService.save(clientInstance)
        } catch (ValidationException e) {
            respond client.errors, view:'create'
            return errors.getFieldError()
        }

        flash.message = message(code: 'default.created.message', args: [
                message(code: 'client.label', default: 'Cliente'),
                clientInstance.getPersonalData().name
        ])
        redirect(action: "index")
    }

    def edit(Long id) {
        ClientB clientInstance = clientService.getById(id)
        respond([clientInstance:clientInstance, personalData:clientInstance.getPersonalData()])
    }

    def update(ClientB clientInstance, PersonalDataBean personalData) {
        clientInstance.setPersonalData(personalData)

        try {
            clientInstance = clientService.update(clientInstance)
        } catch (ValidationException e) {
            respond clientInstance.errors, view:'edit'
            return
        }

        flash.message = message(code: 'default.updated.message', args: [
                message(code: 'client.label', default: 'Cliente'),
                clientInstance.getPersonalData().getName()
        ])
        redirect(action: "index")
    }

    def delete(Long id) {
        if (id == null) {
            notFound()
            return
        }

        clientService.delete(id)

        flash.message = message(code: 'default.deleted.message', args: [
                message(code: 'client.label', default: 'Cliente'),
                id
        ])
        redirect(action: "index")
    }

    protected void notFound() {
        request.withFormat {
            form multipartForm {
                flash.message = message(code: 'default.not.found.message', args: [message(code: 'client.label', default: 'Client'), params.id])
                redirect action: "index", method: "GET"
            }
            '*'{ render status: NOT_FOUND }
        }
    }
}
