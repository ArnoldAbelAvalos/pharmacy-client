package com.healthpotions.beans.category;


import com.healthpotions.beans.base.BaseBean;
import lombok.*;

import java.io.Serial;


public class CategoryB extends BaseBean {


    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;

    public CategoryB(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public CategoryB() {
    }

    public Long getId(){
        return this.id;
    }

    public String get_name() {
        return name;
    }

    public void set_name(String name) {
        this.name = name;
    }
}
