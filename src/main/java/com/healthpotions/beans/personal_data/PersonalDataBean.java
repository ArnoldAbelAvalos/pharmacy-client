package com.healthpotions.beans.personal_data;

import com.healthpotions.beans.base.BaseBean;
import java.time.LocalDate;

public class PersonalDataBean extends BaseBean {
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String lastName;
    private Long document;
    private String ruc;
    private String address;
    private LocalDate dob;
    private String phone;
    private String email;

    public PersonalDataBean(Long id, String name, String lastName, Long document, String ruc, String address, LocalDate dob, String phone, String email) {
        this.id = id;
        this.name = name;
        this.lastName = lastName;
        this.document = document;
        this.ruc = ruc;
        this.address = address;
        this.dob = dob;
        this.phone = phone;
        this.email = email;
    }
    public PersonalDataBean(){

    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public Long getDocument() {
        return document;
    }

    public void setDocument(Long document) {
        this.document = document;
    }

    public String getRuc() {
        return ruc;
    }

    public void setRuc(String ruc) {
        this.ruc = ruc;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public LocalDate getDob() {
        return dob;
    }

    public void setDob(LocalDate dob) {
        this.dob = dob;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
