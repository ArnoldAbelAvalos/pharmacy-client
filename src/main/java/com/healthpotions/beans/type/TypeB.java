package com.healthpotions.beans.type;

import com.healthpotions.beans.base.BaseBean;

import java.io.Serial;

public class TypeB extends BaseBean {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;

    public TypeB(Long id, String name) {
        this.id = id;
        this.name = name;
    }

    public TypeB() {
    }

    public Long getId(){
        return this.id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(Long id) {
        this.id = id;
    }
}