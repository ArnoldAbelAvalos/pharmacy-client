package com.healthpotions.beans.purchase_invoice;

import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.beans.saleInvoice.SaleInvoiceB;

import java.io.Serial;

public class PurchaseInvoiceDetailBean extends BaseBean {
    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private PurchaseInvoiceBean purchaseInvoice;
    private ProductBean product;
    private Long quantity;
    private Long unitPrice;
    private Long total;

    public PurchaseInvoiceDetailBean() {
    }

    public PurchaseInvoiceDetailBean(Long id, PurchaseInvoiceBean purchaseInvoice, ProductBean product, Long quantity, Long unitPrice, Long total) {
        this.id = id;
        this.purchaseInvoice = purchaseInvoice;
        this.product = product;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PurchaseInvoiceBean getPurchaseInvoice() {
        return purchaseInvoice;
    }

    public void setPurchaseInvoice(PurchaseInvoiceBean purchaseInvoice) {
        this.purchaseInvoice = purchaseInvoice;
    }

    public ProductBean getProduct() {
        return product;
    }

    public void setProduct(ProductBean product) {
        this.product = product;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public Long getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Long unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }
}
