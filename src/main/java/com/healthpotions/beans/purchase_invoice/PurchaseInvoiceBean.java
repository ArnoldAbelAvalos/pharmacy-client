package com.healthpotions.beans.purchase_invoice;

import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.supplier.SupplierB;

import java.io.Serial;
import java.time.LocalDateTime;
import java.util.List;

public class PurchaseInvoiceBean extends BaseBean {
    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private SupplierB supplier;
    private String condition;
    private Long number;
    private String stamped;
    private String status;
    private LocalDateTime date;
    private String paymentMethod;
    private Long total;
    private List<PurchaseInvoiceDetailBean> details;

    public PurchaseInvoiceBean() {
    }

    public PurchaseInvoiceBean(Long id, SupplierB supplier, String condition, Long number, String stamped, String status, String paymentMethod, Long total, List<PurchaseInvoiceDetailBean> details, LocalDateTime date) {
        this.id = id;
        this.supplier = supplier;
        this.condition = condition;
        this.number = number;
        this.stamped = stamped;
        this.status = status;
        this.paymentMethod = paymentMethod;
        this.total = total;
        this.details = details;
        this.date = date;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SupplierB getSupplier() {
        return supplier;
    }

    public void setSupplier(SupplierB supplier) {
        this.supplier = supplier;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getStamped() {
        return stamped;
    }

    public void setStamped(String stamped) {
        this.stamped = stamped;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Long getTotal() {
        return total;
    }

    public void setTotal(Long total) {
        this.total = total;
    }

    public List<PurchaseInvoiceDetailBean> getDetails() {
        return details;
    }

    public void setDetails(List<PurchaseInvoiceDetailBean> details) {
        this.details = details;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
