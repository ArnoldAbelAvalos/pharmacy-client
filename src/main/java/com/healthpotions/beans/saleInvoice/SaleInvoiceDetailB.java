package com.healthpotions.beans.saleInvoice;

import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.product.ProductBean;

import java.io.Serial;

public class SaleInvoiceDetailB  extends BaseBean {
    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private SaleInvoiceB saleInvoice;
    private ProductBean product;
    private Integer quantity;
    private Integer unitPrice;
    private Integer total;

    public SaleInvoiceDetailB(Long id, SaleInvoiceB saleInvoice, ProductBean product, Integer quantity, Integer unitPrice, Integer total) {
        this.id = id;
        this.saleInvoice = saleInvoice;
        this.product = product;
        this.quantity = quantity;
        this.unitPrice = unitPrice;
        this.total = total;
    }

    public SaleInvoiceDetailB() {
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SaleInvoiceB getSaleInvoice() {
        return saleInvoice;
    }

    public void setSaleInvoice(SaleInvoiceB saleInvoice) {
        this.saleInvoice = saleInvoice;
    }

    public ProductBean getProduct() {
        return product;
    }

    public void setProduct(ProductBean product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public Integer getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(Integer unitPrice) {
        this.unitPrice = unitPrice;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }
}
