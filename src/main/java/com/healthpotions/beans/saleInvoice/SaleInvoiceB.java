package com.healthpotions.beans.saleInvoice;

import com.healthpotions.beans.client.ClientB;
import com.healthpotions.beans.saleInvoice.SaleInvoiceDetailB;
import com.healthpotions.beans.base.BaseBean;

import java.io.Serial;
import java.util.List;

public class SaleInvoiceB extends BaseBean {

    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private ClientB client;
    private String invoiceDate;
    private Long number;
    private String stamping;
    private String condition;
    private String status;
    private String paymentMethod;
    private Integer total;
    private List<SaleInvoiceDetailB> saleInvoiceDetails;

    public SaleInvoiceB(Long id, ClientB client, String invoiceDate, Long number, String stamping, String condition, String status, String paymentMethod, Integer total, List<SaleInvoiceDetailB> saleInvoiceDetails) {
        this.id = id;
        this.client = client;
        this.invoiceDate = invoiceDate;
        this.number = number;
        this.stamping = stamping;
        this.condition = condition;
        this.status = status;
        this.paymentMethod = paymentMethod;
        this.total = total;
        this.saleInvoiceDetails = saleInvoiceDetails;
    }

    public SaleInvoiceB() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ClientB getClient() {
        return client;
    }

    public void setClient(ClientB client) {
        this.client = client;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public Long getNumber() {
        return number;
    }

    public void setNumber(Long number) {
        this.number = number;
    }

    public String getStamping() {
        return stamping;
    }

    public void setStamping(String stamping) {
        this.stamping = stamping;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(String paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<SaleInvoiceDetailB> getSaleInvoiceDetails() {
        return saleInvoiceDetails;
    }

    public void setSaleInvoiceDetails(List<SaleInvoiceDetailB> saleInvoiceDetails) {
        this.saleInvoiceDetails = saleInvoiceDetails;
    }
}
