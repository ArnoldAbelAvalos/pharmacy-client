package com.healthpotions.beans.role;

import com.healthpotions.beans.base.BaseBean;

public class RoleBean extends BaseBean {
    private static final long serialVersionUID = 1L;

    private Long id;
    private String name;
    private String description;

    public RoleBean() {
    }

    public RoleBean(Long id, String name, String description) {
        this.id = id;
        this.name = name;
        this.description = description;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
