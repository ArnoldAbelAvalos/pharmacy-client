package com.healthpotions.beans.client;

import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.personal_data.PersonalDataBean;

public class ClientB extends BaseBean {

    private Long id;
    private PersonalDataBean personalData;

    public ClientB() {
    }

    public ClientB(Long id, PersonalDataBean personalData) {
        this.id = id;
        this.personalData = personalData;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public PersonalDataBean getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalDataBean personalData) {
        this.personalData = personalData;
    }
}