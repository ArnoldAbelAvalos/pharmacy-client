package com.healthpotions.beans.user;

import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.personal_data.PersonalDataBean;
import com.healthpotions.beans.role.RoleBean;

import java.util.List;

public class UserBean extends BaseBean {
    private static final long serialVersionUID = 1L;


    private Long id;
    private String userName;
    private String password;
    private PersonalDataBean personalData;
    private List<RoleBean> roles;

    public UserBean() {
    }

    public UserBean(Long id, String userName, String password, PersonalDataBean personalData, List<RoleBean> roles) {
        this.id = id;
        this.userName = userName;
        this.password = password;
        this.personalData = personalData;
        this.roles = roles;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public PersonalDataBean getPersonalData() {
        return personalData;
    }

    public void setPersonalData(PersonalDataBean personalData) {
        this.personalData = personalData;
    }

    public List<RoleBean> getRoles() {
        return roles;
    }

    public void setRoles(List<RoleBean> roles) {
        this.roles = roles;
    }
}
