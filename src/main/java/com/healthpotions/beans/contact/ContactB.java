package com.healthpotions.beans.contact;

import com.healthpotions.beans.base.BaseBean;

import java.time.LocalDate;

public class ContactB extends BaseBean {

    private static final long serialVersionUID = 1L;
    private Long id;
    private String fullname;
    private String phone;
    private String email;
    private String reason;
    private String message;

    public ContactB() {
    }

    public ContactB(Long id, String fullname, String phone, String email, String reason, String message) {
        this.id = id;
        this.fullname = fullname;
        this.phone = phone;
        this.email = email;
        this.reason = reason;
        this.message = message;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFullname() {
        return fullname;
    }

    public void setFullname(String fullname) {
        this.fullname = fullname;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}