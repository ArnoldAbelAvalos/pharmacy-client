package com.healthpotions.beans.order;

import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.supplier.SupplierB;

import java.io.Serial;
import java.time.LocalDateTime;
import java.util.List;

public class OrderB extends BaseBean {
    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private SupplierB supplier;
    private String status;
    private List<OrderDetailB> details;
    private LocalDateTime date;

    public OrderB(Long id, SupplierB supplier, String status, List<OrderDetailB> details, LocalDateTime date){
        this.id = id;
        this.supplier = supplier;
        this.status = status;
        this.details = details;
        this.date = date;

    }
    public OrderB() {}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public SupplierB getSupplier() {
        return supplier;
    }

    public void setSupplier(SupplierB supplier) {
        this.supplier = supplier;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<OrderDetailB> getDetails() {
        return details;
    }

    public void setDetails(List<OrderDetailB> details) {
        this.details = details;
    }

    public LocalDateTime getDate() {
        return date;
    }

    public void setDate(LocalDateTime date) {
        this.date = date;
    }
}
