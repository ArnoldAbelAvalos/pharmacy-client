package com.healthpotions.beans.order;

import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.product.ProductBean;

import java.io.Serial;

public class OrderDetailB extends BaseBean {
    @Serial
    private static final long serialVersionUID = 1L;

    private Long id;
    private ProductBean product;
    private Integer quantity;
    private OrderB order;

    public OrderDetailB(Long id, ProductBean product, Integer quantity, OrderB order){
        this.id = id;
        this.product = product;
        this.quantity = quantity;
        this.order = order;
    }

    public OrderDetailB() {
    }

    public Long getId() {
        return id;
    }

    public ProductBean getProduct() {
        return product;
    }

    public void setProduct(ProductBean product) {
        this.product = product;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public OrderB getOrder() {
        return order;
    }

    public void setOrder(OrderB order) {
        this.order = order;
    }
}
