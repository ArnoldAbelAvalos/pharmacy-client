package com.healthpotions.beans.product;

import com.distribuidos.healthpotions.dto.category.CategoryDTO;
import com.healthpotions.beans.base.BaseBean;
import com.healthpotions.beans.category.CategoryB;
import com.healthpotions.beans.type.TypeB;

import java.io.Serial;
import java.time.LocalDate;

public class ProductBean extends BaseBean {

    @Serial
    private static final long serialVersionUID = 1L;
    private Long id;
    private String name;
    private String barcode;
    private Integer costPrice;
    private Integer price;
    private CategoryB category;
    private TypeB type;
    private Boolean requirePrescription;

    public ProductBean(){}

    public ProductBean(Long id, String name, String barcode, Integer costPrice, Integer price, CategoryB category, TypeB type, Boolean requirePrescription){
        this.id = id;
        this.name = name;
        this.barcode = barcode;
        this.costPrice = costPrice;
        this.price = price;
        this.category = category;
        this.type = type;
        this.requirePrescription = requirePrescription;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getBarcode() {
        return barcode;
    }

    public Integer getCostPrice() {
        return costPrice;
    }

    public Integer getPrice() {
        return price;
    }

    public CategoryB getCategory() {return category;}

    public void setCategory(CategoryB category) {this.category = category;}

    public TypeB getType() {return type;}

    public void setType(TypeB type) {this.type = type;}

    public Boolean getRequirePrescription() {
        return requirePrescription;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setBarcode(String barcode) {
        this.barcode = barcode;
    }

    public void setCostPrice(Integer costPrice) {
        this.costPrice = costPrice;
    }

    public void setPrice(Integer price) {
        this.price = price;
    }

    public void setRequirePrescription(Boolean requirePrescription) {
        this.requirePrescription = requirePrescription;
    }
}
