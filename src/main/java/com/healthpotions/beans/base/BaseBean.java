package com.healthpotions.beans.base;

import groovy.transform.Canonical;


import java.io.Serializable;


@Canonical
public abstract class BaseBean implements Serializable {
    private static final long serialVersionUID = 1L;

}