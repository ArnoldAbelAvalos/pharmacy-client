package com.healthpotions.resources.saleInvoice;

import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDetailsDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository("saleInvoiceDetailResource")
public class SaleInvoiceDetailResourceImpl extends BaseResourceImpl<SaleInvoiceDetailsDTO> implements ISaleInvoiceDetailResource {
    public SaleInvoiceDetailResourceImpl() {
        super(SaleInvoiceDetailsDTO.class, "sale_invoices_details");
    }
}
