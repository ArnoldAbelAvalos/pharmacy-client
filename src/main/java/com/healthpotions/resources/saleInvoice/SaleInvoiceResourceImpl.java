package com.healthpotions.resources.saleInvoice;

import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDTO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.healthpotions.dto.RestResponsePage;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.LinkedHashMap;

@Repository("saleInvoiceResource")
public class SaleInvoiceResourceImpl extends BaseResourceImpl<SaleInvoiceDTO> implements ISaleInvoiceResource{
    public SaleInvoiceResourceImpl() {
        super(SaleInvoiceDTO.class, "sale_invoices");
    }

    public Page<LinkedHashMap<String, Object>> getNotDeletedWithQuery(Long page, String query){
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath + "?page=" + page + "&number=" + query))
                .GET().build();
        Page<LinkedHashMap<String, Object>> result = null;
        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == io.micronaut.http.HttpStatus.OK.getCode()){
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                result = mapper.readValue(response.body(), RestResponsePage.class);
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    @Override
    public void cancel(Long id) {
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath + "/cancel/" + id))
                .DELETE().build();
        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}
