package com.healthpotions.resources.saleInvoice;

import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDTO;
import com.healthpotions.resources.base.IBaseResource;
import org.springframework.data.domain.Page;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.LinkedHashMap;

public interface ISaleInvoiceResource extends IBaseResource<SaleInvoiceDTO> {

    public default void cancel(Long id) {
    }
}
