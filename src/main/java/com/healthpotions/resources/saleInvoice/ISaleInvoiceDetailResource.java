package com.healthpotions.resources.saleInvoice;

import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDetailsDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface ISaleInvoiceDetailResource extends IBaseResource<SaleInvoiceDetailsDTO> {

}
