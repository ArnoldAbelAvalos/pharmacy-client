package com.healthpotions.resources.category;

import com.distribuidos.healthpotions.dto.base.BaseDTO;
import com.distribuidos.healthpotions.dto.category.CategoryDTO;
import com.healthpotions.resources.base.IBaseResource;
import org.springframework.data.domain.Page;

import java.io.IOException;


public interface ICategoryResource extends IBaseResource<CategoryDTO> {


}