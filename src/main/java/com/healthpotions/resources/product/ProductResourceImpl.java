package com.healthpotions.resources.product;

import com.distribuidos.healthpotions.dto.product.ProductDTO;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.dto.RestResponsePage;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.LinkedHashMap;

@Repository("productResource")
public class ProductResourceImpl extends BaseResourceImpl<ProductDTO> implements IProductResource {

    public ProductResourceImpl() {
        super(ProductDTO.class, "products");
    }

    public Page<LinkedHashMap<String, Object>> getNotDeletedWithQuery(Long page, String query){
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath + "?page=" + page + "&name=" + query))
                .GET().build();
        Page<LinkedHashMap<String, Object>> result = null;
        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == io.micronaut.http.HttpStatus.OK.getCode()){
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                result = mapper.readValue(response.body(), RestResponsePage.class);
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }

        return result;
    }
}
