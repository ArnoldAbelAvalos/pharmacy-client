package com.healthpotions.resources.product;

import com.distribuidos.healthpotions.dto.product.ProductDTO;
import com.healthpotions.resources.base.IBaseResource;
import org.springframework.data.domain.Page;

public interface IProductResource extends IBaseResource<ProductDTO> {

}
