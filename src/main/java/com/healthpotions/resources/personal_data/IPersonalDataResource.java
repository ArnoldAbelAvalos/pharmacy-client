package com.healthpotions.resources.personal_data;

import com.distribuidos.healthpotions.dto.personal_data.PersonalDataDTO;
import com.healthpotions.resources.base.IBaseResource;
import org.springframework.data.domain.Page;

public interface IPersonalDataResource extends IBaseResource<PersonalDataDTO> {
}
