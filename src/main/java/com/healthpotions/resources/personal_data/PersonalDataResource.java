package com.healthpotions.resources.personal_data;


import com.distribuidos.healthpotions.dto.personal_data.PersonalDataDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository
public class PersonalDataResource extends BaseResourceImpl<PersonalDataDTO> implements IPersonalDataResource {
    public PersonalDataResource() {
        super(PersonalDataDTO.class, "personal_data");
    }
}
