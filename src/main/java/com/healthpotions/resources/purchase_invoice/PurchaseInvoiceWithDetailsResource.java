package com.healthpotions.resources.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository("purchaseInvoiceWithDetailResource")
public class PurchaseInvoiceWithDetailsResource extends BaseResourceImpl<PurchaseInvoiceDTO> implements IPurchaseInvoiceWithDetailsResource {
    public PurchaseInvoiceWithDetailsResource() {
        super(PurchaseInvoiceDTO.class, "purchase_invoice/details");
    }
}
