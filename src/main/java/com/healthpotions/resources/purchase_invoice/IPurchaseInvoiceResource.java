package com.healthpotions.resources.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IPurchaseInvoiceResource extends IBaseResource<PurchaseInvoiceDTO> {
}
