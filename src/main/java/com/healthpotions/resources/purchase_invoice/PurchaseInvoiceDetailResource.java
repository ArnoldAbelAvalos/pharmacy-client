package com.healthpotions.resources.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDetailDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository("purchaseInvoiceDetailResource")
public class PurchaseInvoiceDetailResource extends BaseResourceImpl<PurchaseInvoiceDetailDTO> implements IPurchaseInvoiceDetailResource {
    public PurchaseInvoiceDetailResource() {
        super(PurchaseInvoiceDetailDTO.class, "purchase_invoice_detail");
    }
}
