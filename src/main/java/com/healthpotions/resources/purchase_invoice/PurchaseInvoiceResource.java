package com.healthpotions.resources.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository("purchaseInvoiceResource")
public class PurchaseInvoiceResource extends BaseResourceImpl<PurchaseInvoiceDTO> implements IPurchaseInvoiceResource {
    public PurchaseInvoiceResource() {
        super(PurchaseInvoiceDTO.class, "purchase_invoice");
    }
}
