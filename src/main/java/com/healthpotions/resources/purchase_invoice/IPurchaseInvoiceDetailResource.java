package com.healthpotions.resources.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDetailDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IPurchaseInvoiceDetailResource extends IBaseResource<PurchaseInvoiceDetailDTO> {
}
