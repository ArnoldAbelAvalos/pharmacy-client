package com.healthpotions.resources.order;

import com.distribuidos.healthpotions.dto.order.OrderDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IOrderResource extends IBaseResource<OrderDTO> {
}
