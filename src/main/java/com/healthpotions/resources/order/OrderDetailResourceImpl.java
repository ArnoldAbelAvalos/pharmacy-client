package com.healthpotions.resources.order;

import com.distribuidos.healthpotions.dto.order.OrderDetailDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository ("orderDetailResource")
public class OrderDetailResourceImpl extends BaseResourceImpl<OrderDetailDTO> implements IOrderDetailResource {
    public OrderDetailResourceImpl() {
        super(OrderDetailDTO.class, "orderDetails");
    }
}
