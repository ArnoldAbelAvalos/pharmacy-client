package com.healthpotions.resources.order;

import com.distribuidos.healthpotions.dto.order.OrderDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository("orderWithDetailResource")
public class OrderWithDetailsResource extends BaseResourceImpl<OrderDTO> implements IOrderWithDetailsResource {
    public OrderWithDetailsResource() {
        super(OrderDTO.class, "orders/details");
    }
}
