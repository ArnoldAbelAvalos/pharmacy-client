package com.healthpotions.resources.order;

import com.distribuidos.healthpotions.dto.order.OrderDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository ("orderResource")
public class OrderResourceImpl extends BaseResourceImpl<OrderDTO> implements IOrderResource {
    public OrderResourceImpl() {
        super(OrderDTO.class, "orders");
    }
}
