package com.healthpotions.resources.order;

import com.distribuidos.healthpotions.dto.order.OrderDetailDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IOrderDetailResource extends IBaseResource<OrderDetailDTO> {
}
