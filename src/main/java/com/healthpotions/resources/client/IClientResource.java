package com.healthpotions.resources.client;

import com.distribuidos.healthpotions.dto.client.ClientDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IClientResource extends IBaseResource<ClientDTO> {
}
