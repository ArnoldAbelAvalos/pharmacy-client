package com.healthpotions.resources.client;

import com.distribuidos.healthpotions.dto.client.ClientDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.util.LinkedHashMap;
@Repository("clientResource")
public class ClientResourceImpl extends BaseResourceImpl<ClientDTO> implements IClientResource{

    public ClientResourceImpl() {
        super(ClientDTO.class, "clients");

    }
}
