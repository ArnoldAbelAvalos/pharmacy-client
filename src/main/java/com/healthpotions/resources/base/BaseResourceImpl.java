package com.healthpotions.resources.base;


import com.distribuidos.healthpotions.dto.base.BaseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateSerializer;
import com.healthpotions.dto.RestResponsePage;
import org.springframework.data.domain.Page;
import org.springframework.http.HttpStatus;
import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.LinkedHashMap;

public abstract class BaseResourceImpl<DTO extends BaseDTO> implements IBaseResource<DTO> {
    protected final String resourcePath;
    protected final Class<DTO> dtoClass;
    protected final String BASE_URL = "http://localhost:8080/api/";
    protected final ObjectMapper mapper;
    private final HttpClient httpClient;

    public BaseResourceImpl(Class<DTO> dtoClass, String resourcePath) {
        this.dtoClass = dtoClass;
        this.resourcePath = BASE_URL + resourcePath;
        httpClient = HttpClient.newBuilder()
                .version(HttpClient.Version.HTTP_2)
                .connectTimeout(Duration.ofSeconds(10))
                .build();
        mapper = new ObjectMapper();
        JavaTimeModule module = new JavaTimeModule();
        module.addDeserializer(LocalDate.class, new LocalDateDeserializer(DateTimeFormatter.ISO_DATE));
        module.addSerializer(LocalDate.class, new LocalDateSerializer(DateTimeFormatter.ISO_DATE));
        mapper.registerModule(module);
    }

    protected HttpClient getHttpClient() {
        return this.httpClient;
    }

    protected Class<DTO> getDtoClass() {
        return dtoClass;
    }

    @Override
    public DTO save(DTO dto){
        String json = "";
        try{json = mapper.writeValueAsString(dto);}
        catch (JsonProcessingException e) {
            //TODO
            System.out.println(e.getMessage());
        }
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(json)).build();
        DTO result = null;
        try{
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == HttpStatus.CREATED.value()){
                result = mapper.readValue(response.body(), dtoClass);
            }
            else{
                //TODO
            }
        }
        catch (IOException | InterruptedException e) {
            //TODO
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public DTO getById(Long id) {
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath + "/" +id))
                .GET().build();

        DTO result = null;
        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == HttpStatus.OK.value()){
                result = mapper.readValue(response.body(), dtoClass);
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return result;
    }

    public Page<LinkedHashMap<String, Object>> getNotDeleted(Long page) {
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath + "?page=" + page))
                .GET().build();
        Page<LinkedHashMap<String, Object>> result = null;
        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == io.micronaut.http.HttpStatus.OK.getCode()){
                mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
                result = mapper.readValue(response.body(), RestResponsePage.class);
            }
        } catch (IOException | InterruptedException e) {
            System.out.println(e.getMessage());
        }

        return result;
    }

    @Override
    public DTO update(DTO dto, Long id){
        String json = "";
        try{json = mapper.writeValueAsString(dto);}
        catch (JsonProcessingException e) {
            //TODO
            System.out.println(e.getMessage());
        }
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath + "/"+ id))
                .header("Content-Type", "application/json")
                .PUT(HttpRequest.BodyPublishers.ofString(json)).build();
        DTO result = null;
        try{
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == HttpStatus.OK.value()){
                result = mapper.readValue(response.body(), dtoClass);
            }
            else{
                //TODO
            }
        }
        catch (IOException | InterruptedException e) {
            //TODO
            System.out.println(e.getMessage());
        }
        return result;
    }

    @Override
    public void delete(Long id) {
        HttpRequest request = HttpRequest.newBuilder(URI.create(resourcePath + "/" + id))
                .DELETE().build();
        try {
            HttpResponse<String> response = getHttpClient().send(request, HttpResponse.BodyHandlers.ofString());

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}