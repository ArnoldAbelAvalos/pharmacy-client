package com.healthpotions.resources.base;


import com.distribuidos.healthpotions.dto.base.BaseDTO;
import com.fasterxml.jackson.core.JsonProcessingException;
import org.springframework.data.domain.Page;

import java.util.LinkedHashMap;

import java.io.IOException;

public interface IBaseResource<DTO extends BaseDTO> {

	public DTO save(DTO dto);

	public DTO update(DTO dto, Long id);

	public DTO getById(Long id);

//	public Page<DTO> getAll(Integer page);

	public Page<LinkedHashMap<String, Object>> getNotDeleted(Long page);

	public void delete(Long id);

}
