package com.healthpotions.resources.contact;

import com.distribuidos.healthpotions.dto.contact.ContactDTO;
import com.healthpotions.resources.base.BaseResourceImpl;

public class ContactResourceImpl extends BaseResourceImpl<ContactDTO> implements IContactResource {
    public ContactResourceImpl() {
        super(ContactDTO.class, "contacts");
    }
}