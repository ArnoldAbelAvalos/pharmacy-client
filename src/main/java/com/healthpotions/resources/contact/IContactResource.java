package com.healthpotions.resources.contact;

import com.distribuidos.healthpotions.dto.contact.ContactDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IContactResource extends IBaseResource<ContactDTO> {

}
