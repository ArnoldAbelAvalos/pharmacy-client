package com.healthpotions.resources.supplier;

import com.distribuidos.healthpotions.dto.supplier.SupplierDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface ISupplierResource extends IBaseResource<SupplierDTO> {
}