package com.healthpotions.resources.supplier;

import com.distribuidos.healthpotions.dto.supplier.SupplierDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import org.springframework.stereotype.Repository;

@Repository("supplierResource")
public class SupplierResourceImpl extends BaseResourceImpl<SupplierDTO> implements ISupplierResource {
    public SupplierResourceImpl() {
        super(SupplierDTO.class, "suppliers");
    }
}
