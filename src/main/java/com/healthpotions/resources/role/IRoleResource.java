package com.healthpotions.resources.role;

import com.distribuidos.healthpotions.dto.role.RoleDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IRoleResource extends IBaseResource<RoleDTO> {
}
