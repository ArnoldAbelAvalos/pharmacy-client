package com.healthpotions.resources.role;

import com.distribuidos.healthpotions.dto.role.RoleDTO;
import com.healthpotions.resources.base.BaseResourceImpl;

public class RoleResource extends BaseResourceImpl<RoleDTO> implements IRoleResource {
    public RoleResource() {
        super(RoleDTO.class, "role");
    }
}
