package com.healthpotions.resources.user;

import com.distribuidos.healthpotions.dto.user.UserDTO;
import com.healthpotions.resources.base.IBaseResource;

public interface IUserResource extends IBaseResource<UserDTO> {
}
