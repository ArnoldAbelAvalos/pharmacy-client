package com.healthpotions.resources.type;

import com.distribuidos.healthpotions.dto.personal_data.PersonalDataDTO;
import com.distribuidos.healthpotions.dto.type.TypeDTO;
import com.healthpotions.resources.base.IBaseResource;
import org.springframework.data.domain.Page;

public interface ITypeResource extends IBaseResource<TypeDTO> {


}
