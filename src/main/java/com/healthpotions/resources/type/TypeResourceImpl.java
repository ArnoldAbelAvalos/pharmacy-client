package com.healthpotions.resources.type;

import com.distribuidos.healthpotions.dto.personal_data.PersonalDataDTO;
import com.distribuidos.healthpotions.dto.type.TypeDTO;
import com.healthpotions.resources.base.BaseResourceImpl;
import io.micronaut.http.HttpStatus;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Repository;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Repository("typeResource")
public class TypeResourceImpl extends BaseResourceImpl<TypeDTO> implements ITypeResource {
    public TypeResourceImpl() {
        super(TypeDTO.class, "types");
    }
}
