package com.healthpotions.services.user;

import com.distribuidos.healthpotions.dto.user.UserDTO;
import com.healthpotions.beans.user.UserBean;
import com.healthpotions.services.base.IBaseService;

public interface IUserService extends IBaseService<UserBean, UserDTO> {
}
