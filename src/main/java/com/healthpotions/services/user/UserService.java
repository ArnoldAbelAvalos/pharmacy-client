package com.healthpotions.services.user;

import com.distribuidos.healthpotions.dto.role.RoleDTO;
import com.distribuidos.healthpotions.dto.user.UserDTO;
import com.healthpotions.beans.personal_data.PersonalDataBean;
import com.healthpotions.beans.role.RoleBean;
import com.healthpotions.beans.user.UserBean;
import com.healthpotions.resources.user.IUserResource;
import com.healthpotions.resources.user.UserResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.personal_data.IPersonalDataService;
import com.healthpotions.services.personal_data.PersonalDataService;
import com.healthpotions.services.role.IRoleService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class UserService extends BaseServiceImpl<UserBean, UserDTO> implements IUserService{

    @Autowired
    private UserResource userResource;
    @Autowired
    private PersonalDataService personalDataService;

    @Autowired
    private IRoleService roleService;

    @Autowired
    CacheManager cacheManager;



    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_user_' + #result.id")
    public UserBean save(UserBean bean) {
        PersonalDataBean personalDataBean = personalDataService.save(bean.getPersonalData());
        if (personalDataBean == null){
            //Error
        }
        bean.getPersonalData().setId(personalDataBean.getId());
        UserDTO userDTO = userResource.save(convertBeanToDto(bean));
        return convertDtoToBean(userDTO);
    }

    @Override
    public Pair<List<UserBean>, Long> getAll(Long page) {
        List<UserBean> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  userResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    List<RoleBean> roles = ((ArrayList<Integer>)hashMap.get("roles")).stream()
                            .map(id -> (RoleBean)roleService.getById(Long.valueOf(id)))
                            .toList();
                    result.add(new UserBean(
                            Long.valueOf((Integer) hashMap.get("id")),
                            (String) hashMap.get("userName"),
                            (String) hashMap.get("password"),
                            personalDataService.getById(Long.valueOf((Integer) hashMap.get("personalData"))),
                            roles
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(user-> cacheManager.getCache(Setting.CACHE_NAME).put("client_user_"+user.getId(), user));
        return new Pair<List<UserBean>, Long>(result,totalPages);
    }

    public Pair<List<UserBean>, Long> getAllWithQuery(Long page, String query) {
        List<UserBean> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  userResource.getNotDeletedWithQuery(page, query);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    List<RoleBean> roles = ((ArrayList<Integer>)hashMap.get("roles")).stream()
                            .map(id -> (RoleBean)roleService.getById(Long.valueOf(id)))
                            .toList();
                    result.add(new UserBean(
                            Long.valueOf((Integer) hashMap.get("id")),
                            (String) hashMap.get("userName"),
                            (String) hashMap.get("password"),
                            personalDataService.getById(Long.valueOf((Integer) hashMap.get("personalData"))),
                            roles
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(user-> cacheManager.getCache(Setting.CACHE_NAME).put("client_user_"+user.getId(), user));
        return new Pair<List<UserBean>, Long>(result,totalPages);
    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_user_' + #id")
    public UserBean update(UserBean bean){
        PersonalDataBean personalDataBean = personalDataService.update(bean.getPersonalData());
        if (personalDataBean == null){
            //Error
        }
        bean.setPassword("Aa0000000*");
        UserDTO userDTO = userResource.update(convertBeanToDto(bean), bean.getId());
        return convertDtoToBean(userDTO);
    }

    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_user_' + #id")
    public void delete(Long id){
        userResource.delete(id);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_user_' + #id")
    public UserBean getById(Long id) {
        return convertDtoToBean(userResource.getById(id));
    }


    @Override
    public UserBean convertDtoToBean(UserDTO dto) {
        return new UserBean(
                dto.getId(),
                dto.getUserName(),
                dto.getPassword(),
                personalDataService.getById(dto.getPersonalData()),
                dto.getRoles().stream()
                        .map(role -> roleService.getById(role))
                        .toList()
        );
    }

    @Override
    public UserDTO convertBeanToDto(UserBean bean) {
        return UserDTO.builder()
                .id(bean.getId())
                .userName(bean.getUserName())
                .password(bean.getPassword())
                .personalData(bean.getPersonalData().getId())
                .roles(bean.getRoles().stream()
                        .map(role -> role.getId())
                        .toList())
                .build();
    }
}
