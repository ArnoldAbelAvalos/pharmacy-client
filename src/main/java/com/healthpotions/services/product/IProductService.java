package com.healthpotions.services.product;

import com.distribuidos.healthpotions.dto.product.ProductDTO;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.services.base.IBaseService;

public interface IProductService extends IBaseService<ProductBean, ProductDTO> {

}
