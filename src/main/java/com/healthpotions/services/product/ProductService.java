package com.healthpotions.services.product;

import com.distribuidos.healthpotions.dto.product.ProductDTO;
import com.distribuidos.healthpotions.dto.user.UserDTO;
import com.healthpotions.beans.category.CategoryB;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.resources.product.IProductResource;
import com.healthpotions.resources.product.ProductResourceImpl;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.category.ICategoryService;
import com.healthpotions.services.type.ITypeService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.Cache;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service("productService")
public class ProductService extends BaseServiceImpl<ProductBean, ProductDTO> implements IProductService {

    @Autowired
    ProductResourceImpl productResource;

    @Autowired
    ITypeService typeService;

    @Autowired
    ICategoryService categoryService;

    @Autowired
    CacheManager cacheManager;


    public ProductService(){}

    @Override
    public ProductBean convertDtoToBean(ProductDTO dto) {

        final ProductBean productB = new ProductBean(
                dto.getId(),
                dto.getName(),
                dto.getBarcode(),
                dto.getCostPrice(),
                dto.getPrice(),
                categoryService.getById(dto.getCategory()),
                typeService.getById(dto.getType()),
                dto.getRequirePrescription()
        );

        return productB;
    }

    @Override
    public ProductDTO convertBeanToDto(ProductBean bean) {
        if (bean == null) return null;
        return ProductDTO.builder()
                .name(bean.getName())
                .barcode(bean.getBarcode())
                .costPrice(bean.getCostPrice())
                .price(bean.getPrice())
                .category(bean.getCategory().getId())
                .type(bean.getType().getId())
                .requirePrescription(bean.getRequirePrescription())
                .build();
    }

    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_product_' + #result.id")
    public ProductBean save(ProductBean bean) {
        ProductDTO dto = productResource.save(convertBeanToDto(bean));
        return convertDtoToBean(dto);
    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_product_' + #id")
    public ProductBean update(ProductBean bean, Long id) {
        final ProductDTO dto = productResource.update(convertBeanToDto(bean), id);
        return convertDtoToBean(dto);
    }

    @Override
    public Pair<List<ProductBean>, Long> getAll(Long page) {
        List<ProductBean> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  productResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new ProductBean(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("name"),
                            (String)hashMap.get("barcode"),
                            Integer.valueOf((Integer) hashMap.get("costPrice")),
                            Integer.valueOf((Integer) hashMap.get("price")),
                            categoryService.getById(Long.valueOf((Integer) hashMap.get("category"))),
                            typeService.getById(Long.valueOf((Integer) hashMap.get("type"))),
                            (Boolean)hashMap.get("requirePrescription")
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(product-> cacheManager.getCache(Setting.CACHE_NAME).put("client_product_"+product.getId(), product));
        return new Pair<List<ProductBean>, Long>(result,totalPages);
    }

    public Pair<List<ProductBean>, Long> getAllWithQuery(Long page, String query) {
        List<ProductBean> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  productResource.getNotDeletedWithQuery(page, query);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new ProductBean(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("name"),
                            (String)hashMap.get("barcode"),
                            Integer.valueOf((Integer) hashMap.get("costPrice")),
                            Integer.valueOf((Integer) hashMap.get("price")),
                            categoryService.getById(Long.valueOf((Integer) hashMap.get("category"))),
                            typeService.getById(Long.valueOf((Integer) hashMap.get("type"))),
                            (Boolean)hashMap.get("requirePrescription")
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(product-> cacheManager.getCache(Setting.CACHE_NAME).put("client_product_"+product.getId(), product));
        return new Pair<List<ProductBean>, Long>(result,totalPages);
    }

    @Override
    public ProductBean getById(Long id) {
        Cache.ValueWrapper dto_cached = cacheManager.getCache(Setting.CACHE_NAME).get("client_product_"+id);
        if(dto_cached != null) {
            ProductBean product = (ProductBean) dto_cached.get();
            return product;
        }
        return convertDtoToBean(productResource.getById(id));
    }

    @Override
    public void delete(Long id) {
        cacheManager.getCache(Setting.CACHE_NAME).evictIfPresent("client_product_"+id);
        productResource.delete(id);
    }
}
