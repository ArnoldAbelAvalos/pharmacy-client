package com.healthpotions.services.contact;

import com.distribuidos.healthpotions.dto.contact.ContactDTO;
import com.healthpotions.beans.contact.ContactB;
import com.healthpotions.services.base.IBaseService;

public interface IContactService extends IBaseService<ContactB, ContactDTO> {
}