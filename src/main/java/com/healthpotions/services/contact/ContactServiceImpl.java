package com.healthpotions.services.contact;

import com.distribuidos.healthpotions.dto.contact.ContactDTO;
import com.healthpotions.beans.contact.ContactB;
import com.healthpotions.resources.contact.IContactResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service("contactService")
public class ContactServiceImpl extends BaseServiceImpl<ContactB, ContactDTO> implements IContactService{

    @Autowired
    private IContactResource contactResource;

    @Autowired
    CacheManager cacheManager;


    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_contact_' + #result.id")
    public ContactB save(ContactB bean) {
        return convertDtoToBean(contactResource.save(convertBeanToDto(bean)));
    }

    @Override
    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_contact_' + #id")
    public void delete(Long id) {
        contactResource.delete(id);
    }

    @Override
    public Pair<List<ContactB>, Long> getAll(Long page) {
        List<ContactB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  contactResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new ContactB(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("fullname"),
                            (String)hashMap.get("phone"),
                            (String)hashMap.get("email"),
                            (String)hashMap.get("reason"),
                            (String)hashMap.get("message")
                    ));
                });
        result.forEach(contact-> cacheManager.getCache(Setting.CACHE_NAME).put("client_contact_"+contact.getId(), contact));
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        return new Pair<List<ContactB>, Long>(result, totalPages);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_contact_' + #id")
    public ContactB getById(Long id) {
        return convertDtoToBean(contactResource.getById(id));
    }

    @Override
    public ContactB convertDtoToBean(ContactDTO dto) {
        return new ContactB(
                dto.getId(),
                dto.getFullname(),
                dto.getReason(),
                dto.getMessage(),
                dto.getPhone(),
                dto.getEmail()
        );
    }

    @Override
    public ContactDTO convertBeanToDto(ContactB bean) {
        return ContactDTO.builder()
                .fullname(bean.getFullname())
                .reason(bean.getReason())
                .message(bean.getMessage())
                .phone(bean.getPhone())
                .email(bean.getEmail())
                .build();
    }
}