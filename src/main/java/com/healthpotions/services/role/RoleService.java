package com.healthpotions.services.role;

import com.distribuidos.healthpotions.dto.role.RoleDTO;
import com.healthpotions.beans.role.RoleBean;
import com.healthpotions.resources.role.IRoleResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

public class RoleService extends BaseServiceImpl<RoleBean, RoleDTO> implements IRoleService {

    @Autowired
    private IRoleResource roleResource;

    @Autowired
    CacheManager cacheManager;


    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_role_' + #result.id")
    public RoleBean save(RoleBean bean) {
        return convertDtoToBean(roleResource.save(convertBeanToDto(bean)));
    }

    public RoleBean update(RoleBean bean, Long id) {
        return null;
    }

    @Override
    public Pair<List<RoleBean>, Long> getAll(Long page) {
        List<RoleBean> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  roleResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new RoleBean(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("name"),
                            (String)hashMap.get("description")));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(role-> cacheManager.getCache(Setting.CACHE_NAME).put("client_role_"+role.getId(), role));
        return new Pair<List<RoleBean>, Long>(result, totalPages);
    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_role_' + #id")
    public RoleBean update(RoleBean bean){
        return convertDtoToBean(roleResource.update(convertBeanToDto(bean),bean.getId()));

    }

    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_role_' + #id")
    public void delete(Long id){
        roleResource.delete(id);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_role_' + #id")
    public RoleBean getById(Long id) {
        return convertDtoToBean(roleResource.getById(id));
    }

    @Override
    public RoleBean convertDtoToBean(RoleDTO dto) {
        return new RoleBean(
                dto.getId(),
                dto.getName(),
                dto.getDescription()
        );
    }

    @Override
    public RoleDTO convertBeanToDto(RoleBean bean) {
        return RoleDTO.builder()
                .name(bean.getName())
                .description(bean.getDescription())
                .build();
    }
}
