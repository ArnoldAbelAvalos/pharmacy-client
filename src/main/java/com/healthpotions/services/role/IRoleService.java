package com.healthpotions.services.role;

import com.distribuidos.healthpotions.dto.role.RoleDTO;
import com.healthpotions.beans.role.RoleBean;
import com.healthpotions.services.base.IBaseService;

public interface IRoleService extends IBaseService<RoleBean, RoleDTO> {
}
