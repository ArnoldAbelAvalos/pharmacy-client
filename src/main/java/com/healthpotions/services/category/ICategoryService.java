package com.healthpotions.services.category;

import com.distribuidos.healthpotions.dto.category.CategoryDTO;
import com.healthpotions.beans.category.CategoryB;
import com.healthpotions.services.base.IBaseService;

public interface ICategoryService extends IBaseService<CategoryB, CategoryDTO> {
}
