package com.healthpotions.services.category;

import com.distribuidos.healthpotions.dto.category.CategoryDTO;
import com.healthpotions.beans.category.CategoryB;
import com.healthpotions.beans.role.RoleBean;
import com.healthpotions.beans.user.UserBean;
import com.healthpotions.resources.category.CategoryResourceImpl;
import com.healthpotions.resources.category.ICategoryResource;
import com.healthpotions.services.base.BaseServiceImpl;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import com.healthpotions.utils.Setting;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class CategoryServiceImpl extends BaseServiceImpl<CategoryB, CategoryDTO> implements ICategoryService{
    @Autowired
    private CategoryResourceImpl categoryResource;

    @Autowired
    CacheManager cacheManager;

    @Override
    public CategoryB convertDtoToBean(CategoryDTO dto){
        final CategoryB categoryB = new CategoryB(dto.getId(),dto.getName());
        return categoryB;
    }

    @Override
    public CategoryDTO convertBeanToDto(CategoryB bean) {
        return CategoryDTO.builder()
                .name(bean.get_name())
                .build();
    }

    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_category_' + #result.id")
    public CategoryB save(CategoryB bean) {
        CategoryDTO dto = categoryResource.save(convertBeanToDto(bean));
        return convertDtoToBean(dto);
    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_category_' + #id")
    public CategoryB update(CategoryB bean, Long id) {
        final CategoryDTO dto = categoryResource.update(convertBeanToDto(bean), id);
        return convertDtoToBean(dto);
    }

    @Override
    public Pair<List<CategoryB>, Long> getAll(Long page) {
        List<CategoryB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  categoryResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new CategoryB(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("name")
                    ));

                });
        result.forEach(category-> cacheManager.getCache(Setting.CACHE_NAME).put("client_category_"+category.getId(), category));
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        return new Pair<List<CategoryB>, Long>(result,totalPages);
    }

    public Pair<List<CategoryB>, Long> getAllWithQuery(Long page, String query) {
        List<CategoryB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  categoryResource.getNotDeletedWithQuery(page, query);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new CategoryB(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("name")
                    ));

                });
        result.forEach(category-> cacheManager.getCache(Setting.CACHE_NAME).put("client_category_"+category.getId(), category));
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        return new Pair<List<CategoryB>, Long>(result,totalPages);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_category_' + #id")
    public CategoryB getById(Long id) {
        return convertDtoToBean(categoryResource.getById(id));
    }

   @Override
   @CacheEvict(value = Setting.CACHE_NAME, key = "'client_category_' + #id")
    public void delete(Long id) {
        categoryResource.delete(id);
    }

}