package com.healthpotions.services.client;

import com.distribuidos.healthpotions.dto.client.ClientDTO;
import com.healthpotions.beans.client.ClientB;
import com.healthpotions.services.base.IBaseService;

public interface IClientService extends IBaseService<ClientB, ClientDTO> {
}
