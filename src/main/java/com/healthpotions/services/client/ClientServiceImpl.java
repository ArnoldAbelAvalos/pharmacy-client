package com.healthpotions.services.client;

import com.distribuidos.healthpotions.dto.client.ClientDTO;
import com.healthpotions.beans.client.ClientB;
import com.healthpotions.beans.personal_data.PersonalDataBean;
import com.healthpotions.resources.client.IClientResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.personal_data.IPersonalDataService;
import com.healthpotions.services.personal_data.PersonalDataService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service("clientService")
public class ClientServiceImpl extends BaseServiceImpl<ClientB, ClientDTO> implements IClientService {

    @Autowired
    private IClientResource clientResource;

    @Autowired
    private PersonalDataService personalDataService;

    @Autowired
    CacheManager cacheManager;


    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_client_' + #result.id")
    public ClientB save(ClientB bean) {
        PersonalDataBean personalDataBean = personalDataService.save(bean.getPersonalData());
        if (personalDataBean == null){
            //Error
        }
        bean.getPersonalData().setId(personalDataBean.getId());
        ClientDTO clientDTO = clientResource.save(convertBeanToDto(bean));
        return convertDtoToBean(clientDTO);
    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_client_' + #id")
    public ClientB update(ClientB bean){
        PersonalDataBean personalDataBean = personalDataService.update(bean.getPersonalData());
        if (personalDataBean == null){
            //Error
        }
        ClientDTO clientDTO = clientResource.update(convertBeanToDto(bean), bean.getId());
        return  convertDtoToBean(clientDTO);
    }

    @Override
    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_client_' + #id")
    public void delete(Long id) {

        clientResource.delete(id);
    }

    @Override
    public Pair<List<ClientB>, Long> getAll(Long page) {
        List<ClientB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  clientResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new ClientB(
                            Long.valueOf((Integer) hashMap.get("id")),
                            personalDataService.getById(Long.valueOf((Integer) hashMap.get("personalData")))
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(client-> cacheManager.getCache(Setting.CACHE_NAME).put("client_client_"+client.getId(), client));
        return new Pair<List<ClientB>, Long>(result,totalPages);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_client_' + #id")
    public ClientB getById(Long id) {

        return convertDtoToBean(clientResource.getById(id));
    }

    @Override
    public ClientB convertDtoToBean(ClientDTO dto) {
        return new ClientB(
                dto.getId(),
                personalDataService.getById(dto.getPersonalData())
        );
    }

    @Override
    public ClientDTO convertBeanToDto(ClientB bean) {
        return ClientDTO.builder()
                .id(bean.getId())
                .personalData(bean.getPersonalData().getId())
                .build();
    }
}
