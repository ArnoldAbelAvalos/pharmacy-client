package com.healthpotions.services.type;

import com.distribuidos.healthpotions.dto.type.TypeDTO;
import com.healthpotions.beans.category.CategoryB;
import com.healthpotions.beans.type.TypeB;
import com.healthpotions.resources.type.ITypeResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.*;

@Service("typeService")
public class TypeService extends BaseServiceImpl<TypeB, TypeDTO> implements ITypeService {

    @Autowired
    private ITypeResource typeResource;

    @Autowired
    CacheManager cacheManager;


    public TypeService(){}

    @Override
    public TypeB convertDtoToBean(TypeDTO dto) {
        return new TypeB(
                dto.getId(),
                dto.getName()
        );
    }

    @Override
    public TypeDTO convertBeanToDto(TypeB bean) {
        if (bean == null) return null;
        return TypeDTO.builder()
                .name(bean.getName())
                .build();
    }

    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_type_' + #result.id")
    public TypeB save(TypeB bean) {
        TypeDTO dto = typeResource.save(convertBeanToDto(bean));
        return convertDtoToBean(dto);
    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_type_' + #id")
    public TypeB update(TypeB bean, Long id) {
       TypeDTO dto = typeResource.update(convertBeanToDto(bean), id);
       return  convertDtoToBean(dto);
    }


    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_type_' + #id")
    public TypeB getById(Long id) {
        return convertDtoToBean(typeResource.getById(id));
    }

    @Override
    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_type_' + #id")
    public void delete(Long id){
        typeResource.delete(id);
    }

    @Override
    public Pair<List<TypeB>, Long> getAll(Long page) {
        List<TypeB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  typeResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new TypeB(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("name")
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(type-> cacheManager.getCache(Setting.CACHE_NAME).put("client_type_"+type.getId(), type));
        return new Pair<List<TypeB>, Long>(result,totalPages);
    }


}
