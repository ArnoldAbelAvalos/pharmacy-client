package com.healthpotions.services.type;

import com.distribuidos.healthpotions.dto.type.TypeDTO;
import com.healthpotions.beans.type.TypeB;
import com.healthpotions.services.base.IBaseService;
import org.springframework.data.domain.Page;

public interface ITypeService extends IBaseService<TypeB, TypeDTO> {

}
