package com.healthpotions.services.order;

import com.distribuidos.healthpotions.dto.order.OrderDetailDTO;
import com.healthpotions.beans.order.OrderDetailB;
import com.healthpotions.services.base.IBaseService;

public interface IOrderDetailService extends IBaseService<OrderDetailB, OrderDetailDTO> {
}
