package com.healthpotions.services.order;

import com.distribuidos.healthpotions.dto.order.OrderDetailDTO;
import com.healthpotions.beans.order.OrderB;
import com.healthpotions.beans.order.OrderDetailB;
import com.healthpotions.beans.saleInvoice.SaleInvoiceB;
import com.healthpotions.resources.order.OrderDetailResourceImpl;
import com.healthpotions.services.base.BaseServiceImpl;

import com.healthpotions.services.product.IProductService;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("orderDetailService")
public class OrderDetailServiceImpl extends BaseServiceImpl<OrderDetailB, OrderDetailDTO> implements IOrderDetailService {
    @Autowired
    OrderDetailResourceImpl orderDetailResource;

    @Autowired
    IProductService productService;

    @Autowired
    IOrderService orderService;

    @Override
    public OrderDetailB convertDtoToBean(OrderDetailDTO dto) {
        final OrderDetailB detailB = new OrderDetailB(
                dto.getId(),
                productService.getById(dto.getProduct()),
                dto.getQuantity(),
                null
                );
        return detailB;

    }

    @Override
    public OrderDetailDTO convertBeanToDto(OrderDetailB bean) {
        if (bean == null) return null;
        return OrderDetailDTO.builder()
                .id(bean.getId())
                .product(bean.getProduct().getId())
                .quantity(bean.getQuantity())
                .order(bean.getOrder().getId())
                .build();
    }

    @Override
    public OrderDetailB save(OrderDetailB bean) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Pair<List<OrderDetailB>, Long> getAll(Long page) {
        return null;
    }

    @Override
    public OrderDetailB getById(Long id) {
        return null;
    }
}