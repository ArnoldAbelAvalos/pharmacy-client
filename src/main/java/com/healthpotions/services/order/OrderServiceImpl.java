package com.healthpotions.services.order;

import com.distribuidos.healthpotions.dto.order.OrderDTO;
import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDTO;
import com.healthpotions.beans.client.ClientB;
import com.healthpotions.beans.order.OrderB;
import com.healthpotions.beans.order.OrderDetailB;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.beans.saleInvoice.SaleInvoiceB;
import com.healthpotions.beans.saleInvoice.SaleInvoiceDetailB;
import com.healthpotions.beans.supplier.SupplierB;
import com.healthpotions.resources.order.IOrderWithDetailsResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.product.IProductService;
import com.healthpotions.services.supplier.ISupplierService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;
import com.healthpotions.resources.order.IOrderResource;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service("orderService")
public class OrderServiceImpl extends BaseServiceImpl<OrderB, OrderDTO> implements IOrderService {

    @Autowired
    IOrderResource orderResource;

    @Autowired
    IOrderWithDetailsResource orderWithDetailsResource;

    @Autowired
    OrderDetailServiceImpl orderDetailService;

    @Autowired
    ISupplierService supplierService;

    @Autowired
    IProductService productService;

    @Autowired
    CacheManager cacheManager;

    @Override
    public OrderB convertDtoToBean(OrderDTO dto) {
        if (dto == null) return null;
        return new OrderB(
                dto.getId(),
                supplierService.getById(dto.getSupplier()),
                dto.getStatus(),
                dto.getDetails().stream()
                        .map(id -> orderDetailService.getById(id))
                        .toList(),
                dto.getCreatedAt().toLocalDateTime()
        );
    }

    @Override
    public OrderDTO convertBeanToDto(OrderB bean) {
       return null;
    }

    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_order_' + #result.id")
    public OrderB save(OrderB bean) {
        final OrderDTO classname = convertBeanToDto(bean);
        classname.setDetails(List.of());
        return convertDtoToBean(orderResource.save(classname));
    }

    @Override
    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_order_' + #id")
    public void delete(Long id) {
        orderResource.delete(id);
    }

    @Override
    public Pair<List<OrderB>, Long> getAll(Long page) {
        List<OrderB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage = orderWithDetailsResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach(master -> {
                    List<OrderDetailB> details = ((ArrayList<LinkedHashMap<String, Object>>) master.get("details"))
                            .stream()
                            .map(detail -> {
                                OrderB order = new OrderB();
                                order.setId(Long.valueOf((Integer) detail.get("order")));
                                ProductBean product = new ProductBean();
                                product.setId(Long.valueOf((Integer) detail.get("product")));

                                return new OrderDetailB(
                                        Long.valueOf((Integer) detail.get("id")),
                                        productService.getById(Long.valueOf((Integer) detail.get("product"))),
                                        Integer.valueOf((Integer) detail.get("quantity")),
                                        order
                                );
                            }).toList();

                    result.add(new OrderB(
                            Long.valueOf((Integer) master.get("id")),
                            supplierService.getById(Long.valueOf((Integer) master.get("supplier"))),
                            (String) master.get("status"),
                            details,
                            (Timestamp.from(Instant.parse((String) master.get("createdAt")))).toLocalDateTime()
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());

        result.stream().forEach(order -> {cacheManager.getCache(Setting.CACHE_NAME).put("client_order_" + order.getId(), order);
        });

        return new Pair<>(result, totalPages);
    }


    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_order_' + #id")
    public OrderB getById(Long id) {
       return convertDtoToBean(orderResource.getById(id));
    }
}
