package com.healthpotions.services.order;

import com.distribuidos.healthpotions.dto.order.OrderDTO;
import com.healthpotions.beans.order.OrderB;
import com.healthpotions.services.base.IBaseService;

public interface IOrderService extends IBaseService<OrderB, OrderDTO> {
}
