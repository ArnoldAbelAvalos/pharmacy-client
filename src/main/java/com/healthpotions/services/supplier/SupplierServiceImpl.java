package com.healthpotions.services.supplier;

import com.distribuidos.healthpotions.dto.client.ClientDTO;
import com.distribuidos.healthpotions.dto.supplier.SupplierDTO;
import com.healthpotions.beans.client.ClientB;
import com.healthpotions.beans.personal_data.PersonalDataBean;
import com.healthpotions.beans.supplier.SupplierB;
import com.healthpotions.resources.client.IClientResource;
import com.healthpotions.resources.supplier.ISupplierResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.personal_data.IPersonalDataService;
import com.healthpotions.services.personal_data.PersonalDataService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service("supplierService")
public class SupplierServiceImpl extends BaseServiceImpl<SupplierB, SupplierDTO> implements ISupplierService {
    @Autowired
    private ISupplierResource supplierResource;

    @Autowired
    private PersonalDataService personalDataService;

    @Autowired
    CacheManager cacheManager;


    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_supplier_' + #result.id")
    public SupplierB save(SupplierB bean) {
        PersonalDataBean personalDataBean = personalDataService.save(bean.getPersonalData());
        if (personalDataBean == null){
            //Error
        }
        bean.getPersonalData().setId(personalDataBean.getId());
        SupplierDTO supplierDTO = supplierResource.save(convertBeanToDto(bean));
        return convertDtoToBean(supplierDTO);
    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_supplier_' + #id")
    public SupplierB update(SupplierB bean){
        PersonalDataBean personalDataBean = personalDataService.update(bean.getPersonalData());
        if (personalDataBean == null){
            //Error
        }
        SupplierDTO supplierDTO = supplierResource.update(convertBeanToDto(bean), bean.getId());
        return  convertDtoToBean(supplierDTO);
    }

    @Override
    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_supplier_' + #id")
    public void delete(Long id) {
        supplierResource.delete(id);
    }

    @Override
    public Pair<List<SupplierB>, Long> getAll(Long page) {
        List<SupplierB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  supplierResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new SupplierB(
                            Long.valueOf((Integer) hashMap.get("id")),
                            personalDataService.getById(Long.valueOf((Integer) hashMap.get("personalData")))
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(supplier-> cacheManager.getCache(Setting.CACHE_NAME).put("client_supplier_"+supplier.getId(), supplier));
        return new Pair<List<SupplierB>, Long>(result,totalPages);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_supplier_' + #id")
    public SupplierB getById(Long id) {
        return convertDtoToBean(supplierResource.getById(id));
    }

    @Override
    public SupplierB convertDtoToBean(SupplierDTO dto) {

        return new SupplierB(
                dto.getId(),
                personalDataService.getById(dto.getPersonalData())
        );
    }

    @Override
    public SupplierDTO convertBeanToDto(SupplierB bean) {
        return SupplierDTO.builder()
                .id(bean.getId())
                .personalData(bean.getPersonalData().getId())
                .build();
    }
}
