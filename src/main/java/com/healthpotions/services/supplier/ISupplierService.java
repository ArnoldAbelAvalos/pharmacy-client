package com.healthpotions.services.supplier;

import com.distribuidos.healthpotions.dto.supplier.SupplierDTO;
import com.healthpotions.beans.supplier.SupplierB;
import com.healthpotions.services.base.IBaseService;

public interface ISupplierService extends IBaseService<SupplierB, SupplierDTO> {
}