package com.healthpotions.services.base;

import com.distribuidos.healthpotions.dto.base.BaseDTO;
import com.healthpotions.beans.base.BaseBean;
import java.io.IOException;
import java.util.List;

import com.healthpotions.beans.user.UserBean;
import grails.util.Pair;

public interface IBaseService<BEAN extends BaseBean, DTO extends BaseDTO> {
	public BEAN save(BEAN bean);

	public void delete(Long id);

	public Pair<List<BEAN>, Long> getAll(Long page);

	public BEAN getById(Long id);

	public abstract BEAN convertDtoToBean(DTO dto);

	public abstract DTO convertBeanToDto(BEAN bean);
}

