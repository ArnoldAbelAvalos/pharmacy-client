package com.healthpotions.services.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDetailDTO;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.beans.purchase_invoice.PurchaseInvoiceDetailBean;
import com.healthpotions.resources.purchase_invoice.IPurchaseInvoiceDetailResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.product.IProductService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;

import java.util.List;

public class PurchaseInvoiceDetailService extends BaseServiceImpl<PurchaseInvoiceDetailBean, PurchaseInvoiceDetailDTO> implements IPurchaseInvoiceDetailService{

    @Autowired
    IPurchaseInvoiceDetailResource purchaseInvoiceDetailResource;

    @Autowired
    IProductService productService;


    @Override
    public PurchaseInvoiceDetailBean save(PurchaseInvoiceDetailBean bean) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Pair<List<PurchaseInvoiceDetailBean>, Long> getAll(Long page) {
        return null;
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_purchase_invoice_detail_' + #id")
    public PurchaseInvoiceDetailBean getById(Long id) {
        return convertDtoToBean(purchaseInvoiceDetailResource.getById(id));
    }

    @Override
    public PurchaseInvoiceDetailBean convertDtoToBean(PurchaseInvoiceDetailDTO dto) {
        if (dto == null) return null;
        ProductBean product = productService.getById(dto.getProduct());
        return new PurchaseInvoiceDetailBean(
                dto.getId(),
                null,
                product,
                dto.getQuantity(),
                dto.getUnitPrice(),
                dto.getTotal()
        );
    }

    @Override
    public PurchaseInvoiceDetailDTO convertBeanToDto(PurchaseInvoiceDetailBean bean) {
        return null;
    }
}
