package com.healthpotions.services.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDTO;
import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDTO;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.beans.purchase_invoice.PurchaseInvoiceBean;
import com.healthpotions.beans.purchase_invoice.PurchaseInvoiceDetailBean;
import com.healthpotions.resources.purchase_invoice.IPurchaseInvoiceResource;
import com.healthpotions.resources.purchase_invoice.IPurchaseInvoiceWithDetailsResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.product.IProductService;
import com.healthpotions.services.supplier.ISupplierService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class PurchaseInvoiceService extends BaseServiceImpl<PurchaseInvoiceBean, PurchaseInvoiceDTO> implements IPurchaseInvoiceService {
    @Autowired
    IPurchaseInvoiceWithDetailsResource purchaseInvoiceWithDetailsResource;

    @Autowired
    IPurchaseInvoiceResource purchaseInvoiceResource;

    @Autowired
    IPurchaseInvoiceDetailService purchaseInvoiceDetailService;

    @Autowired
    ISupplierService supplierService;

    @Autowired
    IProductService productService;

    @Autowired
    CacheManager cacheManager;


    @Override
    public PurchaseInvoiceBean save(PurchaseInvoiceBean bean) {
        return null;
    }

    @Override
    public void delete(Long id) {
        purchaseInvoiceResource.delete(id);
    }

    @Override
    public Pair<List<PurchaseInvoiceBean>, Long> getAll(Long page) {
        List<PurchaseInvoiceBean> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage = purchaseInvoiceWithDetailsResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach(master -> {
                    List<PurchaseInvoiceDetailBean> details = ((ArrayList<LinkedHashMap<String, Object>>) master.get("details"))
                            .stream()
                            .map(detail -> {
                                PurchaseInvoiceBean purchaseInvoice = new PurchaseInvoiceBean();
                                purchaseInvoice.setId(Long.valueOf((Integer) detail.get("purchaseInvoice")));
                                ProductBean product = new ProductBean();
                                product.setId(Long.valueOf((Integer) detail.get("product")));

                                return new PurchaseInvoiceDetailBean(
                                        Long.valueOf((Integer) detail.get("id")),
                                        purchaseInvoice,
                                        productService.getById(Long.valueOf((Integer) detail.get("product"))),
                                        Long.valueOf((Integer) detail.get("quantity")),
                                        Long.valueOf((Integer) detail.get("unitPrice")),
                                        Long.valueOf((Integer) detail.get("total"))
                                );
                            }).toList();

                    result.add(new PurchaseInvoiceBean(
                            Long.valueOf((Integer) master.get("id")),
                            supplierService.getById(Long.valueOf((Integer) master.get("supplier"))),
                            (String) master.get("condition"),
                            Long.valueOf((Integer) master.get("number")),
                            (String) master.get("stamped"),
                            (String) master.get("status"),
                            (String) master.get("paymentMethod"),
                            Long.valueOf((Integer) master.get("total")),
                            details,
                            (Timestamp.from(Instant.parse((String) master.get("createdAt")))).toLocalDateTime()
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());

        result.stream().forEach(invoice -> {cacheManager.getCache(Setting.CACHE_NAME).put("client_purchase_invoice_" + invoice.getId(), invoice);
        });

        return new Pair<>(result, totalPages);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_purchase_invoice_' + #id")
    public PurchaseInvoiceBean getById(Long id) {
        return convertDtoToBean(purchaseInvoiceResource.getById(id));
    }

    @Override
    public PurchaseInvoiceBean convertDtoToBean(PurchaseInvoiceDTO dto) {
        if (dto == null) return null;
        return new PurchaseInvoiceBean(
                dto.getId(),
                supplierService.getById(dto.getSupplier()),
                dto.getCondition(),
                dto.getNumber(),
                dto.getStamped(),
                dto.getStatus(),
                dto.getPaymentMethod(),
                dto.getTotal(),
                dto.getDetails().stream()
                        .map(id -> purchaseInvoiceDetailService.getById(id))
                        .toList(),
                dto.getCreatedAt().toLocalDateTime()
        );
    }

    @Override
    public PurchaseInvoiceDTO convertBeanToDto(PurchaseInvoiceBean bean) {
        if (bean == null) return null;
        return PurchaseInvoiceDTO.builder()
                .id(bean.getId())
                .supplier(bean.getSupplier().getId())
                .number(bean.getNumber())
                .stamped(bean.getStamped())
                .condition(bean.getCondition())
                .status(bean.getStatus())
                .paymentMethod(bean.getPaymentMethod())
                .total(bean.getTotal())
                .details(
                        bean.getDetails() == null ?
                                List.of()
                                : bean.getDetails().stream().map(
                                detail -> detail.getId()).toList())
                .build();
    }
}
