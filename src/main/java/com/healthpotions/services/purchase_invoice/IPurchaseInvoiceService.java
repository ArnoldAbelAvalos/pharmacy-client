package com.healthpotions.services.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDTO;
import com.healthpotions.beans.purchase_invoice.PurchaseInvoiceBean;
import com.healthpotions.services.base.IBaseService;

public interface IPurchaseInvoiceService extends IBaseService<PurchaseInvoiceBean, PurchaseInvoiceDTO> {
}
