package com.healthpotions.services.purchase_invoice;

import com.distribuidos.healthpotions.dto.purchase_invoice.PurchaseInvoiceDetailDTO;
import com.healthpotions.beans.purchase_invoice.PurchaseInvoiceBean;
import com.healthpotions.beans.purchase_invoice.PurchaseInvoiceDetailBean;
import com.healthpotions.services.base.IBaseService;

public interface IPurchaseInvoiceDetailService extends IBaseService<PurchaseInvoiceDetailBean, PurchaseInvoiceDetailDTO> {
}
