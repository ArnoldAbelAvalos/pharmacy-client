package com.healthpotions.services.saleInvoice;

import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDetailsDTO;
import com.healthpotions.beans.saleInvoice.SaleInvoiceDetailB;
import com.healthpotions.services.base.IBaseService;

public interface ISaleInvoiceDetailService extends IBaseService<SaleInvoiceDetailB, SaleInvoiceDetailsDTO>  {
}
