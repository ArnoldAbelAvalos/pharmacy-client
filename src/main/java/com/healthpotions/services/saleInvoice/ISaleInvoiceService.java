package com.healthpotions.services.saleInvoice;

import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDTO;
import com.healthpotions.beans.saleInvoice.SaleInvoiceB;
import com.healthpotions.services.base.IBaseService;

public interface ISaleInvoiceService extends IBaseService<SaleInvoiceB, SaleInvoiceDTO> {

}
