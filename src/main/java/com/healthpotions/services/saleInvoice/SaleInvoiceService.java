package com.healthpotions.services.saleInvoice;

import com.distribuidos.healthpotions.dto.product.ProductDTO;
import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDTO;
import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDetailsDTO;
import com.healthpotions.beans.client.ClientB;
import com.healthpotions.beans.product.ProductBean;
import com.healthpotions.beans.role.RoleBean;
import com.healthpotions.beans.saleInvoice.SaleInvoiceB;
import com.healthpotions.beans.saleInvoice.SaleInvoiceDetailB;
import com.healthpotions.beans.user.UserBean;
import com.healthpotions.resources.saleInvoice.ISaleInvoiceResource;
import com.healthpotions.resources.saleInvoice.SaleInvoiceResourceImpl;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.client.IClientService;
import com.healthpotions.services.product.IProductService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.List;

@Service("saleInvoiceService")
public class SaleInvoiceService extends BaseServiceImpl<SaleInvoiceB, SaleInvoiceDTO> implements ISaleInvoiceService {

    @Autowired
    SaleInvoiceResourceImpl saleInvoiceResource;

    @Autowired
    SaleInvoiceDetailService saleInvoiceDetailService;

    @Autowired
    IClientService clientService;

    @Autowired
    IProductService productService;

    @Autowired
    CacheManager cacheManager;


    public SaleInvoiceService() {
    }

    @Override
    public SaleInvoiceB convertDtoToBean(SaleInvoiceDTO dto) {
        if (dto == null) return null;
        return  new SaleInvoiceB(
                dto.getId(),
                clientService.getById(dto.getClient()),
                dto.getInvoiceDate(),
                dto.getNumber(),
                dto.getStamping(),
                dto.getCondition(),
                dto.getStatus(),
                dto.getPaymentMethod(),
                dto.getTotal(),
                dto.getSaleInvoiceDetails() == null ?
                                List.of()
                                : dto.getSaleInvoiceDetails().stream().map(
                                detail -> saleInvoiceDetailService.convertDtoToBean(detail)).toList());

    }

    @Override
    public SaleInvoiceDTO convertBeanToDto(SaleInvoiceB bean) {
        if (bean == null) return null;
        return SaleInvoiceDTO.builder()
            .id(bean.getId())
            .client(bean.getClient().getId())
            .invoiceDate(bean.getInvoiceDate())
            .number(bean.getNumber())
            .stamping(bean.getStamping())
            .condition(bean.getCondition())
            .status(bean.getStatus())
            .paymentMethod(bean.getPaymentMethod())
            .total(bean.getTotal())
            .saleInvoiceDetails(
                    bean.getSaleInvoiceDetails() == null ?
                            List.of()
                            : bean.getSaleInvoiceDetails().stream().map(
                            detail -> saleInvoiceDetailService.convertBeanToDto(detail)).toList())
            .build();
    }

    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_sale_invoice_' + #result.id")
    public SaleInvoiceB save(SaleInvoiceB bean) {
        final SaleInvoiceDTO classname = convertBeanToDto(bean);
        classname.setSaleInvoiceDetails(List.of());
        return convertDtoToBean(saleInvoiceResource.save(classname));
    }

    public SaleInvoiceB update(SaleInvoiceB bean, Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {
        saleInvoiceResource.cancel(id);
    }

    @Override
    public Pair<List<SaleInvoiceB>, Long> getAll(Long page) {
        List<SaleInvoiceB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage = saleInvoiceResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach(master -> {
                    List<SaleInvoiceDetailB> details = ((ArrayList<LinkedHashMap<String, Object>>) master.get("saleInvoiceDetails"))
                            .stream()
                            .map(detail -> {
                                SaleInvoiceB saleInvoice = new SaleInvoiceB();
                                saleInvoice.setId(Long.valueOf((Integer) detail.get("saleInvoice")));
                                ProductBean product = new ProductBean();
                                product.setId(Long.valueOf((Integer) detail.get("product")));

                                return new SaleInvoiceDetailB(
                                        Long.valueOf((Integer) detail.get("id")),
                                        saleInvoice,
                                        productService.getById(Long.valueOf((Integer) detail.get("product"))),
                                        Integer.valueOf((Integer) detail.get("quantity")),
                                        Integer.valueOf((Integer) detail.get("unitPrice")),
                                        Integer.valueOf((Integer) detail.get("total"))
                                );
                            }).toList();

                    result.add(new SaleInvoiceB(
                            Long.valueOf((Integer) master.get("id")),
                            clientService.getById(Long.valueOf((Integer) master.get("client"))),
                            (String) master.get("invoiceDate"),
                            Long.valueOf((Integer) master.get("number")),
                            (String) master.get("stamping"),
                            (String) master.get("condition"),
                            (String) master.get("status"),
                            (String) master.get("paymentMethod"),
                            Integer.valueOf((Integer) master.get("total")),
                            details
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());

        result.stream().forEach(invoice -> {cacheManager.getCache(Setting.CACHE_NAME).put("client_sale_invoice_" + invoice.getId(), invoice);
        });

        return new Pair<>(result, totalPages);

    }

    public Pair<List<SaleInvoiceB>, Long> getAllWithQuery(Long page, String query) {
        List<SaleInvoiceB> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage = saleInvoiceResource.getNotDeletedWithQuery(page,query);
        dtoPage.getContent()
                .stream()
                .forEach(master -> {
                    List<SaleInvoiceDetailB> details = ((ArrayList<LinkedHashMap<String, Object>>) master.get("saleInvoiceDetails"))
                            .stream()
                            .map(detail -> {
                                SaleInvoiceB saleInvoice = new SaleInvoiceB();
                                saleInvoice.setId(Long.valueOf((Integer) detail.get("saleInvoice")));
                                ProductBean product = new ProductBean();
                                product.setId(Long.valueOf((Integer) detail.get("product")));

                                return new SaleInvoiceDetailB(
                                        Long.valueOf((Integer) detail.get("id")),
                                        saleInvoice,
                                        product,
                                        Integer.valueOf((Integer) detail.get("quantity")),
                                        Integer.valueOf((Integer) detail.get("unitPrice")),
                                        Integer.valueOf((Integer) detail.get("total"))
                                );
                            }).toList();

                    ClientB client = new ClientB();
                    client.setId(Long.valueOf((Integer) master.get("client")));

                    result.add(new SaleInvoiceB(
                            Long.valueOf((Integer) master.get("id")),
                            clientService.getById(Long.valueOf((Integer) master.get("client"))),
                            (String) master.get("invoiceDate"),
                            Long.valueOf((Integer) master.get("number")),
                            (String) master.get("stamping"),
                            (String) master.get("condition"),
                            (String) master.get("status"),
                            (String) master.get("paymentMethod"),
                            Integer.valueOf((Integer) master.get("total")),
                            details
                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());

        return new Pair<>(result, totalPages);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_sale_invoice_' + #id")
    public SaleInvoiceB getById(Long id) {
        return convertDtoToBean(saleInvoiceResource.getById(id));
    }
}