package com.healthpotions.services.saleInvoice;

import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDTO;
import com.distribuidos.healthpotions.dto.sale_invoices.SaleInvoiceDetailsDTO;
import com.healthpotions.beans.saleInvoice.SaleInvoiceB;
import com.healthpotions.beans.saleInvoice.SaleInvoiceDetailB;
import com.healthpotions.resources.saleInvoice.ISaleInvoiceDetailResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.services.product.IProductService;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CachePut;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.EnableTransactionManagement;



import java.util.List;

@Service("saleInvoiceDetailService")
@EnableTransactionManagement(proxyTargetClass = true)
public class SaleInvoiceDetailService extends BaseServiceImpl<SaleInvoiceDetailB, SaleInvoiceDetailsDTO> implements ISaleInvoiceDetailService{

    @Autowired
    ISaleInvoiceDetailResource saleInvoiceDetailResource;

    @Autowired
    IProductService productService;

    @Autowired
    ISaleInvoiceService saleInvoiceService;


    public SaleInvoiceDetailService() {
    }

    @Override
    public SaleInvoiceDetailB convertDtoToBean(SaleInvoiceDetailsDTO dto) {
        final SaleInvoiceDetailB detailB = new SaleInvoiceDetailB(
                dto.getId(),
                new SaleInvoiceB(),
                productService.getById(dto.getProduct()),
                dto.getQuantity(),
                dto.getUnitPrice(),
                dto.getTotal()
        );

        return detailB;
    }

    @Override
    public SaleInvoiceDetailsDTO convertBeanToDto(SaleInvoiceDetailB bean) {
        if (bean == null) return null;
        return SaleInvoiceDetailsDTO.builder()
                .id(bean.getId())
                .product(bean.getProduct().getId())
                .quantity(bean.getQuantity())
                .unitPrice(bean.getUnitPrice())
                .total(bean.getTotal())
                .saleInvoice(bean.getSaleInvoice().getId())
                .build();
    }

    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_sale_invoice_detail_' + #result.id")
    public SaleInvoiceDetailB save(SaleInvoiceDetailB bean) {
        final SaleInvoiceDetailsDTO classname = convertBeanToDto(bean);
        classname.setTotal(classname.getQuantity() * classname.getUnitPrice());
        final SaleInvoiceDetailsDTO dto = saleInvoiceDetailResource.save(classname);
        final SaleInvoiceDetailB finalConvert = convertDtoToBean(dto);
        return finalConvert;
    }

    public SaleInvoiceDetailB update(SaleInvoiceDetailB bean, Long id) {
        return null;
    }

    @Override
    public void delete(Long id) {

    }

    @Override
    public Pair<List<SaleInvoiceDetailB>, Long> getAll(Long page) {
        return null;
    }

    @Override
    public SaleInvoiceDetailB getById(Long id) {
        return null;
    }
}
