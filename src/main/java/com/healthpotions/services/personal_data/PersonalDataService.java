package com.healthpotions.services.personal_data;

import com.distribuidos.healthpotions.dto.personal_data.PersonalDataDTO;
import com.healthpotions.beans.personal_data.PersonalDataBean;
import com.healthpotions.resources.personal_data.IPersonalDataResource;
import com.healthpotions.services.base.BaseServiceImpl;
import com.healthpotions.utils.Setting;
import grails.util.Pair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

@Service
public class PersonalDataService extends BaseServiceImpl<PersonalDataBean, PersonalDataDTO> implements IPersonalDataService{

    @Autowired
    private IPersonalDataResource personalDataResource;

    @Autowired
    CacheManager cacheManager;

    @Override
    @CachePut(value = Setting.CACHE_NAME, key = "'client_personal_data_' + #result.id")
    public PersonalDataBean save(PersonalDataBean bean) {
        return convertDtoToBean(personalDataResource.save(convertBeanToDto(bean)));
    }

    @Override
    public Pair<List<PersonalDataBean>, Long> getAll(Long page) {
        List<PersonalDataBean> result = new ArrayList<>();
        Page<LinkedHashMap<String, Object>> dtoPage =  personalDataResource.getNotDeleted(page);
        dtoPage.getContent()
                .stream()
                .forEach( hashMap -> {
                    result.add(new PersonalDataBean(
                            Long.valueOf((Integer)hashMap.get("id")),
                            (String)hashMap.get("name"),
                            (String)hashMap.get("lastName"),
                            Long.valueOf((Integer)hashMap.get("document")),
                            (String)hashMap.get("ruc"),
                            (String)hashMap.get("address"),
                            LocalDate.parse((String)hashMap.get("dob")),
                            (String)hashMap.get("phone"),
                            (String)hashMap.get("email")

                    ));
                });
        Long totalPages = Long.valueOf(dtoPage.getTotalPages());
        result.forEach(personalData-> cacheManager.getCache(Setting.CACHE_NAME).put("client_personal_data_"+personalData.getId(), personalData));
        return new Pair<List<PersonalDataBean>, Long>(result, totalPages);

    }

    @CachePut(value = Setting.CACHE_NAME, key = "'client_personal_data_' + #id")
    public PersonalDataBean update(PersonalDataBean bean){
        return convertDtoToBean(personalDataResource.update(convertBeanToDto(bean),bean.getId()));

    }

    @CacheEvict(value = Setting.CACHE_NAME, key = "'client_personal_data_' + #id")
    public void delete(Long id){
        personalDataResource.delete(id);
    }

    @Override
    @Cacheable(value = Setting.CACHE_NAME, key = "'client_personal_data_' + #id")
    public PersonalDataBean getById(Long id) {
        return convertDtoToBean(personalDataResource.getById(id));
    }

    @Override
    public PersonalDataBean convertDtoToBean(PersonalDataDTO dto) {
        return new PersonalDataBean(
                dto.getId(),
                dto.getName(),
                dto.getLastName(),
                dto.getDocument(),
                dto.getRuc(),
                dto.getAddress(),
                dto.getDob(),
                dto.getPhone(),
                dto.getEmail()
        );
    }

    @Override
    public PersonalDataDTO convertBeanToDto(PersonalDataBean bean) {
        return PersonalDataDTO.builder()
                .name(bean.getName())
                .lastName(bean.getLastName())
                .document(bean.getDocument())
                .ruc(bean.getRuc())
                .address(bean.getAddress())
                .dob(bean.getDob())
                .phone(bean.getPhone())
                .email(bean.getEmail())
                .build();
    }
}