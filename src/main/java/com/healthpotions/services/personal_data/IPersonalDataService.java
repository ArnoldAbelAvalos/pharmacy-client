package com.healthpotions.services.personal_data;

import com.distribuidos.healthpotions.dto.personal_data.PersonalDataDTO;
import com.healthpotions.beans.personal_data.PersonalDataBean;
import com.healthpotions.services.base.IBaseService;

public interface IPersonalDataService extends IBaseService<PersonalDataBean, PersonalDataDTO> {
}
